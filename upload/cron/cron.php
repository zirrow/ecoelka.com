<?php
	require_once ('../config.php');

	require_once(DIR_SYSTEM . 'startup.php');

	$db_ee = new DB('mysqli', 'acc00986.mysql.tools', DB_USERNAME, DB_PASSWORD, DB_USERNAME);
	$db_lc = new DB('mysqli', 'acc00986.mysql.tools', 'acc00986_db', 'Tf6Pbc4J', 'acc00986_db');

	//Перенсим таблицы со связанными опциями
	$db_ee->query("TRUNCATE TABLE " . DB_PREFIX . "relatedoptions_discount");

	$sql = "SELECT * FROM lc_relatedoptions_discount";

	$relatedoptions_discount_from_lc = $db_lc->query($sql);

	foreach ($relatedoptions_discount_from_lc->rows as $relatedoption_discount_from_lc){
		$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_discount 
							SET
							relatedoptions_id = '".$relatedoption_discount_from_lc['relatedoptions_id']."',
							customer_group_id = '".$relatedoption_discount_from_lc['customer_group_id']."',
							quantity = '".$relatedoption_discount_from_lc['quantity']."',
							priority = '".$relatedoption_discount_from_lc['priority']."',
							price = '".$relatedoption_discount_from_lc['price']."'
							";

		$db_ee->query($sql);
	}

	$db_ee->query("TRUNCATE TABLE " . DB_PREFIX . "relatedoptions_special");

	$sql = "SELECT * FROM lc_relatedoptions_special";

	$relatedoptions_special_from_lc = $db_lc->query($sql);

	foreach ($relatedoptions_special_from_lc->rows as $relatedoption_special_from_lc){
		$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_special 
								SET
								relatedoptions_id = '".$relatedoption_special_from_lc['relatedoptions_id']."',
								customer_group_id = '".$relatedoption_special_from_lc['customer_group_id']."',
								priority = '".$relatedoption_special_from_lc['priority']."',
								price = '".$relatedoption_special_from_lc['price']."'
								";

		$db_ee->query($sql);
	}

	$db_ee->query("TRUNCATE TABLE " . DB_PREFIX . "relatedoptions_variant");

	$sql = "SELECT * FROM lc_relatedoptions_variant";

	$relatedoptions_variant_from_lc = $db_lc->query($sql);

	foreach ($relatedoptions_variant_from_lc->rows as $relatedoption_variant_from_lc){
		$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_variant 
										SET
										relatedoptions_variant_id = '".$relatedoption_variant_from_lc['relatedoptions_variant_id']."',
										relatedoptions_variant_name = '".$relatedoption_variant_from_lc['relatedoptions_variant_name']."',
										sort_order = '".$relatedoption_variant_from_lc['sort_order']."'
										";

		$db_ee->query($sql);
	}
	//Закончили переносить

	$sql = "SELECT p.*,
			  pt1.`1c_id`,
			  m.name as manuf_name,
			  md.description as manuf_desc,
			  pd.name as pd_name,
			  (SELECT COUNT(attribute_id) FROM lc_product_attribute pa WHERE pa.product_id = p.product_id) as count_attributs,
			  (SELECT COUNT(option_id) FROM lc_product_option pa WHERE pa.product_id = p.product_id) as count_options,
			  (SELECT COUNT(relatedoptions_id) FROM lc_relatedoptions ro WHERE ro.product_id = p.product_id) as count_related_options
			FROM lc_product p
			  LEFT JOIN lc_product_to_1c  pt1 ON p.product_id = pt1.product_id
			  LEFT JOIN lc_manufacturer m ON p.manufacturer_id = m.manufacturer_id
			  LEFT JOIN lc_manufacturer_description md ON m.manufacturer_id = md.manufacturer_id
			  LEFT JOIN lc_product_description pd ON pd.product_id = p.product_id
			WHERE p.status = 1
			  AND md.language_id = 1
			  AND pd.language_id = 1";

	$products_from_lacrema = $db_lc->query($sql);
	$products_from_lacrema = $products_from_lacrema->rows;

	$log = new Log('cron_log.txt');

	foreach ($products_from_lacrema as $product_from_lacrema){
		$sql = "SELECT COUNT(product_id) as count, product_id FROM oc_product WHERE mpn = '". $product_from_lacrema['1c_id'] ."'";

		$count_products_with_1c_id = $db_ee->query($sql);
		$count_products_with_1c_id = $count_products_with_1c_id->row;

		$log->write("Ищим продукт в базе продуктов по sku - " . print_r($count_products_with_1c_id,1));

		if($count_products_with_1c_id['count'] == 0){

			$log->write("Товар не найден. Создаем новый.");

			// Процессинг с производителями.
			$sql = "SELECT manufacturer_id FROM oc_manufacturer_description WHERE name LIKE '%". $product_from_lacrema['manuf_name'] ."%'";

			$manufacturer_id = $db_ee->query($sql);

			if($manufacturer_id->num_rows > 0){
				$manufacturer_id = $manufacturer_id->row['manufacturer_id'];
			} else {
				$db_ee->query("INSERT INTO " . DB_PREFIX . "manufacturer SET sort_order = '99'");

				$manufacturer_id = $db_ee->getLastId();

				$db_ee->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '0'");
				$db_ee->query("INSERT INTO " . DB_PREFIX . "manufacturer_description SET manufacturer_id = '" . (int)$manufacturer_id . "', language_id = '2', name = '" . $db_ee->escape($product_from_lacrema['manuf_name']) . "'");
			}

			$log->write("Id произвдителя - ".$manufacturer_id);
			// Закончили с производителями.

			// Процессинг по созданию продукта
			$db_ee->query("INSERT INTO " . DB_PREFIX . "product SET
									model = '" . $db_ee->escape($product_from_lacrema['model']) . "',
									sku = '" . $db_ee->escape($product_from_lacrema['sku']) . "',
									upc = '" . $db_ee->escape($product_from_lacrema['upc']) . "',
									ean = '" . $db_ee->escape($product_from_lacrema['ean']) . "',
									jan = '" . $db_ee->escape($product_from_lacrema['jan']) . "',
									isbn = '" . $db_ee->escape($product_from_lacrema['isbn']) . "',
									mpn = '" . $db_ee->escape($product_from_lacrema['1c_id']) . "',
									location = '" . $db_ee->escape($product_from_lacrema['location']) . "',
									quantity = '" . (int)$product_from_lacrema['quantity'] . "',
									minimum = '" . (int)$product_from_lacrema['minimum'] . "',
									subtract = '" . (int)$product_from_lacrema['subtract'] . "',
									stock_status_id = '" . (int)$product_from_lacrema['stock_status_id'] . "',
									date_available = '" . $db_ee->escape($product_from_lacrema['date_available']) . "',
									manufacturer_id = '" . (int)$manufacturer_id . "',
									shipping = '" . (int)$product_from_lacrema['shipping'] . "',
									price = '" . (float)$product_from_lacrema['price'] . "',
									points = '" . (int)$product_from_lacrema['points'] . "',
									weight = '" . (float)$product_from_lacrema['weight'] . "',
									weight_class_id = '" . (int)$product_from_lacrema['weight_class_id'] . "',
									length = '" . (float)$product_from_lacrema['length'] . "',
									width = '" . (float)$product_from_lacrema['width'] . "',
									height = '" . (float)$product_from_lacrema['height'] . "',
									length_class_id = '" . (int)$product_from_lacrema['length_class_id'] . "',
									status = '0',
									tax_class_id = '" . (int)$product_from_lacrema['tax_class_id'] . "',
									sort_order = '" . (int)$product_from_lacrema['sort_order'] . "',
									date_added = NOW(),
									date_modified = NOW()");

			$product_id = $db_ee->getLastId();

			$log->write("Id нового продукта - ". $product_id);

			$db_ee->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '2', name = '" . $db_ee->escape($product_from_lacrema['pd_name']) . "'");
			$db_ee->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '0'");
			// Закочили с продуктами

			//Процессинг картинок
			if(!empty($product_from_lacrema['image'])){

				$log->write("Процессинг картинки запущен");

				$old_link = '/home/acc00986/lacrema.com.ua/www/image/'. $product_from_lacrema['image'];
				$new_link = DIR_IMAGE.'catalog/cron/'. $product_from_lacrema['image'];

				if(file_exists($old_link) && !file_exists($new_link)){

					$path = '';

					$directories = explode('/', dirname($new_link));

					foreach ($directories as $directory) {
						$path = $path . '/' . $directory;

						if (!is_dir($path)) {
							@mkdir($path, 0777);
						}
					}

					if(copy($old_link, $new_link)){
						$db_ee->query("UPDATE " . DB_PREFIX . "product SET image = '" . 'catalog/cron/'.$db_ee->escape($product_from_lacrema['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
						$log->write("Картинка создана");
					}
				}
			}
			//Процессинг картинок ЗАКОНЧЕН

			//Процессинг url
			$db_ee->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=". (int)$product_id ."'");
			$db_ee->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '0', language_id = '2', query = 'product_id=" . (int)$product_id . "', keyword = '" . translit($product_from_lacrema['pd_name']) ."-". (int)$product_id . "'");
			//Процессинг url Закончен

		} else {

			//Процессинг по обновлению продуктов.

			$product_id = $count_products_with_1c_id['product_id'];

			$log->write("Продукт найден, и его ID - ". $product_id);

			$db_ee->query("UPDATE " . DB_PREFIX . "product SET price = '" . (float)$product_from_lacrema['price'] . "', quantity = '" . (int)$product_from_lacrema['quantity'] . "' WHERE product_id = '". (int) $product_id ."'");
		}

		// Процессинг с атрибутами
		if ($product_from_lacrema['count_attributs'] > 0) {

			$log->write("Процессинг атрибутов");

			$sql = "SELECT pa.product_id, pa.text, ad.name AS attr_name, agd.name AS attr_gr_name
						FROM lc_product_attribute pa
						  LEFT JOIN lc_attribute a ON pa.attribute_id = a.attribute_id
						  LEFT JOIN lc_attribute_description ad ON ad.attribute_id = a.attribute_id
						  LEFT JOIN lc_attribute_group ag ON ag.attribute_group_id = a.attribute_group_id
						  LEFT JOIN lc_attribute_group_description agd ON agd.attribute_group_id = a.attribute_group_id
						WHERE pa.product_id = '". $product_from_lacrema['product_id'] ."'
						  AND ad.language_id = 1
						  AND agd.language_id = 1";

			$attributes_by_product_from_lacrema = $db_lc->query($sql);
			$attributes_by_product_from_lacrema = $attributes_by_product_from_lacrema->rows;

			$db_ee->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

			foreach ($attributes_by_product_from_lacrema as $attribute_by_product_from_lacrema){

				$sql = "SELECT attribute_group_id FROM " . DB_PREFIX . "attribute_group_description WHERE name LIKE '%". $attribute_by_product_from_lacrema['attr_gr_name'] ."%'";
				$attribute_group_id_from_ee = $db_ee->query($sql);

				if($attribute_group_id_from_ee->num_rows > 0){
					$attribute_group_id_from_ee = $attribute_group_id_from_ee->row['attribute_group_id'];
				} else {
					$db_ee->query("INSERT INTO " . DB_PREFIX . "attribute_group SET sort_order = 99");
					$attribute_group_id_from_ee = $db_ee->getLastId();

					$db_ee->query("INSERT INTO " . DB_PREFIX . "attribute_group_description SET attribute_group_id = '". $attribute_group_id_from_ee ."', language_id = '2', name = '". $db_ee->escape($attribute_by_product_from_lacrema['attr_gr_name']) ."'");
				}

				$sql = "SELECT attribute_id FROM " . DB_PREFIX . "attribute_description WHERE name LIKE '%". $attribute_by_product_from_lacrema['attr_name'] ."%' AND language_id = '2'";
				$attribute_id_from_ee = $db_ee->query($sql);

				if($attribute_id_from_ee->num_rows > 0){
					$attribute_id_from_ee = $attribute_id_from_ee->row['attribute_id'];
				} else {
					$db_ee->query("INSERT INTO " . DB_PREFIX . "attribute SET attribute_group_id = '". $attribute_group_id_from_ee ."', sort_order = 99");
					$attribute_id_from_ee = $db_ee->getLastId();

					$db_ee->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '". $attribute_id_from_ee ."', language_id = '2', name = '". $db_ee->escape($attribute_by_product_from_lacrema['attr_name']) ."'");
				}

				$db_ee->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '". (int)$product_id ."', attribute_id = '". $attribute_id_from_ee ."',language_id = '2', text = '".$db_ee->escape($attribute_by_product_from_lacrema['text'])."'");
			}
		}
		// Закончили с атрибутами

		// процессинг по созданию опций
		if($product_from_lacrema['count_options'] > 0){

			$log->write("Процессинг опций");

			$sql = "SELECT pov.*, o.type, od.name AS option_name, ovd.name AS option_val_name
						FROM lc_product_option_value pov
						  LEFT JOIN lc_option o ON pov.option_id = o.option_id
						  LEFT JOIN lc_option_description od ON pov.option_id = od.option_id
						  LEFT JOIN lc_option_value_description ovd ON pov.option_value_id = ovd.option_value_id
						WHERE pov.product_id = '". $product_from_lacrema['product_id'] ."'
						AND od.language_id = 1
						AND ovd.language_id = 1";

			$options_by_product_from_lacrema = $db_lc->query($sql);
			$options_by_product_from_lacrema = $options_by_product_from_lacrema->rows;

			$db_ee->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
			$db_ee->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

			foreach ($options_by_product_from_lacrema as $option_by_product_from_lacrema){

				$sql = "SELECT * FROM " . DB_PREFIX . "option_description WHERE name LIKE '". $option_by_product_from_lacrema['option_name'] ."'";

				$option_id_from_ee = $db_ee->query($sql);

				if($option_id_from_ee->num_rows > 0){
					$option_id_from_ee = $option_id_from_ee->row['option_id'];
				} else {
					$db_ee->query("INSERT INTO " . DB_PREFIX . "option SET type = '". $option_by_product_from_lacrema['type'] ."', sort_order = 99");
					$option_id_from_ee = $db_ee->getLastId();

					$db_ee->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '". $option_id_from_ee ."', language_id = '2', name = '". $db_ee->escape($option_by_product_from_lacrema['option_name']) ."'");
				}

				$sql = "SELECT * FROM " . DB_PREFIX . "option_value_description WHERE name LIKE '". $option_by_product_from_lacrema['option_val_name'] ."'";

				$option_value_id_from_ee = $db_ee->query($sql);

				if($option_value_id_from_ee->num_rows > 0){
					$option_value_id_from_ee = $option_value_id_from_ee->row['option_value_id'];
				} else {
					$db_ee->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '". $option_id_from_ee ."', sort_order = 99");
					$option_value_id_from_ee = $db_ee->getLastId();

					$db_ee->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '". $option_value_id_from_ee ."', language_id = '2', option_id = ". $option_id_from_ee .", name = '". $db_ee->escape($option_by_product_from_lacrema['option_val_name']) ."'");
				}

				$sql = "SELECT po.product_option_id 
						FROM " . DB_PREFIX . "product_option po 
							LEFT JOIN " . DB_PREFIX . "option_description od ON (po.option_id = od.option_id) 
						WHERE od.name = '". $option_by_product_from_lacrema['option_name'] ."' 
							AND po.product_id = '". (int)$product_id ."'";

				$product_option_id = $db_ee->query($sql);

				if($product_option_id->num_rows > 0){
					$product_option_id = $product_option_id->row['product_option_id'];
				} else {
					$db_ee->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '". (int)$product_id ."', option_id = '". $option_id_from_ee ."', value = '', required = '1'");
					$product_option_id = $db_ee->getLastId();
				}

				$db_ee->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '".$product_option_id."', product_id = '". (int)$product_id ."', option_id = '". $option_id_from_ee ."', option_value_id = '". $option_value_id_from_ee ."', quantity = '".$option_by_product_from_lacrema['quantity']."', subtract = '".$option_by_product_from_lacrema['subtract']."', price = '".$option_by_product_from_lacrema['price']."', price_prefix = '".$option_by_product_from_lacrema['price_prefix']."', points = '".$option_by_product_from_lacrema['points']."', points_prefix = '".$option_by_product_from_lacrema['points_prefix']."', weight = '".$option_by_product_from_lacrema['weight']."', weight_prefix = '".$option_by_product_from_lacrema['weight_prefix']."'");
			}
		}
		// процессинг по созданию опций ЗАВЕРШЕН

		//Процессинг Связанных опций
		if($product_from_lacrema['count_related_options'] > 0){

			$log->write('Запускаем процесс работы со связанными опциями');

			$db_ee->query("DELETE FROM " . DB_PREFIX . "relatedoptions WHERE product_id = '". (int) $product_id ."'");

			$sql = "SELECT * FROM lc_relatedoptions WHERE product_id = '". $product_from_lacrema['product_id'] ."'";

			$relatedoptions_from_lc = $db_lc->query($sql);

			foreach ($relatedoptions_from_lc->rows as $relatedoption_from_lc){
				$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions 
						SET relatedoptions_id = '".$relatedoption_from_lc['relatedoptions_id']."',
						relatedoptions_variant_product_id = '".$relatedoption_from_lc['relatedoptions_variant_product_id']."',
						product_id = '".(int) $product_id."',
						quantity = '".$relatedoption_from_lc['quantity']."',
						model = '".$relatedoption_from_lc['model']."',
						sku = '".$relatedoption_from_lc['sku']."',
						upc = '".$relatedoption_from_lc['upc']."',
						ean = '".$relatedoption_from_lc['ean']."',
						location = '".$relatedoption_from_lc['location']."',
						stock_status_id = '".$relatedoption_from_lc['stock_status_id']."',
						weight_prefix = '".$relatedoption_from_lc['weight_prefix']."',
						weight = '".$relatedoption_from_lc['weight']."',
						price_prefix = '".$relatedoption_from_lc['price_prefix']."',
						price = '".$relatedoption_from_lc['price']."',
						defaultselect = '".$relatedoption_from_lc['defaultselect']."',
						defaultselectpriority = '".$relatedoption_from_lc['defaultselectpriority']."'
						";

				$db_ee->query($sql);
			}

			// Опции связанных опций
			$db_ee->query("DELETE FROM " . DB_PREFIX . "relatedoptions_option WHERE product_id = '". (int) $product_id ."'");

			$sql = "SELECT ro.* , ovd.name
					FROM lc_relatedoptions_option ro 
						LEFT JOIN lc_option_value_description ovd ON (ovd.option_value_id = ro.option_value_id)
					WHERE ro.product_id = '". $product_from_lacrema['product_id'] ."'
						AND ovd.language_id = '1'";

			$relatedoptions_option_from_lc = $db_lc->query($sql);

			foreach ($relatedoptions_option_from_lc->rows as $relatedoption_option_from_lc){
				$sql = "SELECT option_id, option_value_id 
						FROM " . DB_PREFIX . "option_value_description 
						WHERE name = '". $relatedoption_option_from_lc['name'] ."'
							AND language_id = '2'";
				$option_value_description = $db_ee->query($sql);
				$option_value_description = $option_value_description->row;

				$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_option 
						SET relatedoptions_id = '". $relatedoption_option_from_lc['relatedoptions_id'] ."',
						product_id = '". (int) $product_id ."',
						option_id = '". $option_value_description['option_id'] ."',
						option_value_id = '". $option_value_description['option_value_id'] ."'
						";

				$db_ee->query($sql);
			}
			//Опции связанных опций ЗАКОНЧЕНО

			$db_ee->query("DELETE FROM " . DB_PREFIX . "relatedoptions_search WHERE product_id = '". (int) $product_id ."'");

			$sql = "SELECT * FROM lc_relatedoptions_search WHERE product_id = '". $product_from_lacrema['product_id'] ."'";

			$relatedoptions_search_from_lc = $db_lc->query($sql);

			foreach ($relatedoptions_search_from_lc->rows as $relatedoption_search_from_lc){
				$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_search 
						SET
						product_id = '".(int) $product_id."',
						ro_ids = '".$relatedoption_search_from_lc['ro_ids']."',
						model = '".$relatedoption_search_from_lc['model']."',
						sku = '".$relatedoption_search_from_lc['sku']."'
						";

				$db_ee->query($sql);
			}

			$db_ee->query("DELETE FROM " . DB_PREFIX . "relatedoptions_search WHERE product_id = '". (int) $product_id ."'");

			$sql = "SELECT * FROM lc_relatedoptions_search WHERE product_id = '". $product_from_lacrema['product_id'] ."'";

			$relatedoptions_search_from_lc = $db_lc->query($sql);

			foreach ($relatedoptions_search_from_lc->rows as $relatedoption_search_from_lc){
				$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_search 
						SET
						product_id = '".(int) $product_id."',
						ro_ids = '".$relatedoption_search_from_lc['ro_ids']."',
						model = '".$relatedoption_search_from_lc['model']."',
						sku = '".$relatedoption_search_from_lc['sku']."'
						";

				$db_ee->query($sql);
			}

			$db_ee->query("DELETE FROM " . DB_PREFIX . "relatedoptions_variant_product WHERE product_id = '". (int) $product_id ."'");

			$sql = "SELECT * FROM lc_relatedoptions_variant_product WHERE product_id = '". $product_from_lacrema['product_id'] ."'";

			$relatedoptions_variant_product_from_lc = $db_lc->query($sql);

			foreach ($relatedoptions_variant_product_from_lc->rows as $relatedoption_variant_product_from_lc){
				$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_variant_product 
						SET
						relatedoptions_variant_product_id = '".$relatedoption_variant_product_from_lc['relatedoptions_variant_product_id']."',
						relatedoptions_variant_id = '".$relatedoption_variant_product_from_lc['relatedoptions_variant_id']."',
						product_id = '".(int) $product_id."',
						relatedoptions_use = '".$relatedoption_variant_product_from_lc['relatedoptions_use']."'
						";

				$db_ee->query($sql);
			}
		}

		//Процессинг Связанных опций ЗАВЕРШЕН
	}

	//Привязываем обычные опции к связанным
	$db_ee->query("TRUNCATE TABLE " . DB_PREFIX . "relatedoptions_variant_option");

	$sql = "SELECT rvo.* , od.name
				FROM lc_relatedoptions_variant_option rvo
					LEFT JOIN lc_option_description od ON (od.option_id = rvo.option_id)";

	$relatedoptions_variant_option_from_lc = $db_lc->query($sql);

	foreach ($relatedoptions_variant_option_from_lc->rows as $relatedoption_variant_option_from_lc){

		$sql = "SELECT option_id FROM " . DB_PREFIX . "option_description WHERE name = '". $relatedoption_variant_option_from_lc['name'] ."'";

		$option_id = $db_ee->query($sql)->row['option_id'];

		$sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_variant_option 
												SET
												relatedoptions_variant_id = '". $relatedoption_variant_option_from_lc['relatedoptions_variant_id'] ."',
												option_id = '". $option_id ."'
												";

		$db_ee->query($sql);
	}
	//Привязываем обычные опции к связанным ЗАКОНЧИЛИ

	function translit($s) {
		$s = (string) $s; // преобразуем в строковое значение
		$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
		$s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
		$s = trim($s); // убираем пробелы в начале и конце строки
		$s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
		$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
		$s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
		$s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
		return $s; // возвращаем результат
	}