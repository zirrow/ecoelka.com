<?php
// Text
$_['text_subject']      = '%s - Заказ %s';
$_['text_received']     = 'Вы получили заказ.';
$_['text_order_id']     = '№ заказа:';
$_['text_date_added']   = 'Дата заказа:';
$_['text_order_status'] = 'Состояние заказа:';
$_['text_product']      = 'Товары';
$_['text_total']        = 'Итого';
$_['text_comment']      = 'Комментарий к Вашему заказу:';

$_['text_user_data']    = 'Данные о клиенте:';
$_['text_telephone']    = 'телефон - ';
$_['text_name']         = 'ФИО - ';
$_['text_payment_method']    = 'Способ оплаты - ';
$_['text_shipping_method']   = 'Способ доставки - ';
$_['text_shipping_city']     = 'Город - ';
$_['text_shipping_address_1']    = 'Адрес 1';
$_['text_shipping_address_2']    = 'Адрес 2';