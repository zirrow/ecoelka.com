<?php
// Heading  
//$_['heading_title']          = 'SOthemes Cart';

// Text
$_['text_title']             = 'Товар добавлен в корзину';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = '<a href="%s">%s</a> добавлен в <a href="%s">shopping cart</a>!';
$_['text_items']     	     = '<span class="items_cart">%s</span><span class="items_cart2"> товар(ы)</span><span class="items_carts"> - %s </span> ';
$_['text_shop']  			  = 'Совершить покупку сейчас';
$_['text_shop_cart']		  = 'Моя корзина';
// Error
$_['error_required']         = 'требуется %s!';	

?>