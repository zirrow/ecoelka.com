<?php
// Text
$_['text_home']          = 'Главная страница';
$_['text_shopping_cart'] = 'Корзина';
$_['text_account']       = 'Мой аккаунт';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Войти';
$_['text_logout']        = 'Выйти';
$_['text_checkout']      = 'Проверить';
$_['text_search']        = 'Найти';
$_['text_cart']        = 'Корзина';
$_['text_all']           = 'Показать все';
$_['text_more']       = 'Больше';
$_['text_language']       = 'Языки';
$_['text_currency']       = 'Валюта';
$_['text_compare']       = 'Сравнить';
$_['text_itemcount']     = '<span class="items_cart">%s </span>';

$_['text_needhelp']      = 'Нужна помощь';
$_['text_emailus']       = 'Напишите нам';
$_['text_morecategory']       = 'Больше категорий';