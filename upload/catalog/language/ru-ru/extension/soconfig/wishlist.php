<?php
// Heading  
//$_['heading_title']          = 'SOthemes Wish List';

// Text
$_['text_account']  = 'Аккаунт';
$_['text_instock']  = 'В наличии';
$_['text_wishlist'] = 'Список пожеланий (%s)';
$_['text_title']             = 'Продукт добавлен в список пожеланий';
$_['text_failure']           = 'Ошибка';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_exists']            = 'Некоторые товары уже указаны на eBay, поэтому были удалены';
$_['text_success']           = 'Готово: Вы добавили <a href="%s">%s</a> в Ваш <a href="%s">wish list</a>!';
$_['text_login']             = 'Вы должны <a href="%s">login</a> или <a href="%s">создать аккаунт</a> to save <a href="%s">%s</a> в Ваш <a href="%s">wish list</a>!';
$_['text_remove']   	= 'Готово: Вы изменили свой список пожеланий!';
$_['text_empty']    	= 'Ваш список пожеланий пуст.';
?>