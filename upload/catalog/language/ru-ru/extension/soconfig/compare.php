<?php
// Heading  
//$_['heading_title']          = 'SOthemes Compare';

// Text
$_['text_title']             = 'Товар добавлен в сравнение';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = 'Готово: Вы добавили <a href="%s">%s</a> в Ваш <a href="%s">product comparison</a>!';
$_['text_warning']           = 'Максимальное количество продуктов для сравнения - 4. Первый добавленный продукт был удален из списка сравнения.';
$_['text_exists']            = 'Некоторые товары уже добавлены в список сравнения';

$_['text_items']             = '%s';
$_['text_compare']   	   	 = 'Сравнить (%s)';
$_['text_failure']           = 'Ошибка';
// Error
$_['error_required']         = 'Требуется %s!';	

?>