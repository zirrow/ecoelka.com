<?php
// Heading
$_['heading_title'] = 'Сортировать по';

// Text
$_['text_tax']      		= 'Без НДС:';
$_['text_noproduct']      	= 'Нет товаров!';
$_['text_sale']      		= 'Распродажа';
$_['text_new']      		= 'Новое';

$_['text_search']      		= 'Поиск';
$_['text_price']      		= 'Цена';
$_['text_reset_all']      	= 'Сбросить все';
$_['text_manufacturer']     = 'Производитель';
$_['text_subcategory']     	= 'Подкатегория';
$_['button_cart']     		= 'Добавить в корзину';
$_['button_compare']        = 'Сравнить';
$_['button_wishlist']       = 'Список желаний';
$_['button_quickview']      = 'Быстрый просмотр';
