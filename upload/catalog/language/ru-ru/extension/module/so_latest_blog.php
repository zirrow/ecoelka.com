<?php
// Heading

$_['text_latest']  		= 'Последнее'; 
$_['text_comment']     	= ' Комментарий';
$_['text_comments']    	= ' Комментарии';
$_['text_view']        	= ' Просмотр';
$_['text_views']       	= ' Просмотры';
// Text
$_['text_none_author'] 	= 'Нет автора';
$_['text_tax']      	= 'Без НДС:';
$_['text_noitem']      	= 'Нет товаров!';
$_['text_no_database'] 	= 'Отсутствует таблица базы данных для этого расширения, пожалуйста, установите «Simple Blog»!';