<?php
// Text
$_['text_search']    	= 'Ключевое слово...';
$_['text_category_all'] = 'Все категории';
$_['text_tax']      	= 'Налог';
$_['text_price']      	= 'Цена';
$_['button_cart']       = 'Добавить в корзину';
$_['button_wishlist']   = 'Добавить в список желаний';
$_['button_compare']    = 'Добавить в сравнение';
$_['text_btn_search']   = 'Поиск';
