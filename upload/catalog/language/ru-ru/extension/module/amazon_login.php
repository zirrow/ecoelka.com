<?php
// Heading
$_['heading_title'] = 'Войти через Amazon';

// Text
$_['text_module'] = 'Модули';
$_['text_success'] = 'Готово: Вы изменили модуль входа через Amazon!';
$_['text_content_top'] = 'Вверху';
$_['text_content_bottom'] = 'Внизу';
$_['text_column_left'] = 'Слева';
$_['text_column_right'] = 'Справа';
$_['text_lwa_button'] = 'Войти через Amazon';
$_['text_login_button'] = 'Вход';
$_['text_a_button'] = 'A';
$_['text_gold_button'] = 'Золотая';
$_['text_darkgray_button'] = 'Темно серая';
$_['text_lightgray_button'] = 'Светло серая';
$_['text_small_button'] = 'Маленькая';
$_['text_medium_button'] = 'Средняя';
$_['text_large_button'] = 'Большая';
$_['text_x_large_button'] = 'Очень большая';

//Entry
$_['entry_button_type'] = 'Тип кнопки';
$_['entry_button_colour'] = 'Цвет кнопки';
$_['entry_button_size'] = 'Размер кнопки';
$_['entry_layout'] = 'Макет';
$_['entry_position'] = 'Размещение';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Порядок сортировки';

//Error
$_['error_permission'] = 'Внимание: У вас нет разрешения на изменение модуля входа через Amazon!';