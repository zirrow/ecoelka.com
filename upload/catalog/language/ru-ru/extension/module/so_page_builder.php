<?php
// Heading
//$_['heading_title'] = 'Редактор страниц';

// Text
$_['text_tax']      		= 'Без НДС:';
$_['text_noproduct']      	= 'Нет товаров!';

$_['shortcode_email']   			= 'Email';
$_['shortcode_email_desc']			= 'Введите свой админский email чтобы получить email.';
$_['shortcode_email_validation']   	= 'Требуется Email';
$_['shortcode_email_validation_']	= 'Пожалуйста, введите действительный e-mail';
$_['shortcode_name']   				= 'Имя';
$_['shortcode_name_desc']			= 'Введите здесь имя. которое Вы хотете видеть в заголовке.';
$_['shortcode_name_validation']   	= 'Требуется имя';
$_['shortcode_message']   			= 'Сообщение';
$_['shortcode_message_validation']  = 'Требуется сообщение';
$_['shortcode_subject']   			= 'Тема';
$_['shortcode_subject_desc']		= 'Если вы выберете «да», тогда отобразите поле темы контактной формы.';
$_['shortcode_subject_validation']  = 'Требуется тема';
$_['shortcode_send_success']  		= 'Ваше сообщение было успешно отправлено';
$_['shortcode_send_error']  		= 'Ошибка обработки сервера';
$_['shortcode_carousel_not_item_desc']= 'Содержание карусели не найдено, проверьте настройки источника карусели.';