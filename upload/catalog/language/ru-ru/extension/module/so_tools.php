<?php

$_['text_history']			= 'История';
$_['text_shopping_cart']	= 'Корзина';
$_['text_register']			= 'Регистр';
$_['text_account']			= 'Счет';
$_['text_download']			= 'Загрузка';
$_['text_login']			= 'Вход';
$_['text_recent_products']	= 'Недавно просмотренные товары';
$_['text_my_account']		= 'Мой аккаунт';
$_['text_new']				= 'Новое';
$_['text_items_product']	= 'В Вашей корзине <span class="text-color">%s item(s)</span>';
$_['text_items']     	    = '<span class="items_cart">%s</span><span class="items_cart2"> товар(s)</span><span class="items_carts"> - %s </span> ';
$_['button_cart']			= 'Добавить в корзину';
$_['text_search']			= 'Поиск';
$_['text_all_categories']	= 'Все категории';
$_['text_head_categories']	= 'Категории';
$_['text_head_cart']		= 'Корзина';
$_['text_head_account']		= 'Счет';
$_['text_head_search']		= 'Поиск';
$_['text_head_recent_view']	= 'Недавно просмотренные';
$_['text_head_gotop']		= 'Вверх';
$_['text_empty']               = 'Ваша корзина пуста!';
