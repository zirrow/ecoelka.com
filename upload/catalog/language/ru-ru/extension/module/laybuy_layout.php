<?php
// Heading
$_['heading_title']            = 'Информация о Lay-Buy';

// Text
$_['text_reference_info']      = 'Справка';
$_['text_laybuy_ref_no']       = 'Ссылка на Lay-Buy ID:';
$_['text_paypal_profile_id']   = 'ID профиля PayPal:';
$_['text_payment_plan']        = 'План оплаты';
$_['text_status']              = 'Статус:';
$_['text_amount']              = 'Количество:';
$_['text_downpayment_percent'] = 'Процент авансовых платежей:';
$_['text_months']              = 'Месяцы:';
$_['text_downpayment_amount']  = 'Сумма авансового платежа:';
$_['text_payment_amounts']     = 'Суммы платежей:';
$_['text_first_payment_due']   = 'Первый платеж:';
$_['text_last_payment_due']    = 'Последний платеж:';
$_['text_downpayment']         = 'Первоначальный взнос';
$_['text_month']               = 'Месяц';

// Column
$_['column_instalment']        = 'Взнос';
$_['column_amount']            = 'Количество';
$_['column_date']              = 'Дата';
$_['column_pp_trans_id']       = 'ID транзакции PayPal';
$_['column_status']            = 'Статус';