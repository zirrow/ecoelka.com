<?php
// Heading
$_['heading_title'] = 'Предложения';

// Text
$_['text_tax']      		= 'Без НДС:';
$_['text_noitem']      		= 'Нет товаров!';
$_['text_sale']      		= 'Распродажа';
$_['text_new']      		= 'Новое';
$_['text_time_left']      		= '<span>Поторопись!</span> Предложение заканчивается:';

$_['text_Day']      		= 'День';
$_['text_Hour']      		= 'Час';
$_['text_Min']      		= 'Минута';
$_['text_Sec']      		= 'Секунда';
$_['text_Days']      		= 'Дни';
$_['text_Hours']      		= 'Часы';
$_['text_Mins']      		= 'Минуты';
$_['text_Secs']      		= 'Секунды';

$_['text_viewall']       = 'Смотреть все';
$_['text_available']    = 'Доступно:';
$_['text_sold']       	= 'Продано:';
