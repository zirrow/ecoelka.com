<?php
// Heading 
$_['heading_title']					= 'Посты';

// Buttons
$_['button_continue_reading']		= 'Читать больше';
$_['button_submit']					= 'ОСТАВИТЬ КОММЕНТАРИЙ';

$_['text_date_format']				= '<b>d</b> M';
$_['text_date_format_long']			= 'F jS, Y  g:i A';

// Entry
$_['entry_name']					= 'Имя';
$_['entry_captcha']					= 'Введите код в поле ниже:';
$_['entry_review']					= 'Комментарий';

// Text
$_['text_home']						= 'Главная страница';
$_['text_no_found']					= 'В этой категории нет постов!';
$_['text_related_product']			= 'Сопутствующие товары';
$_['text_related_comment']			= 'Комментарии';
$_['text_related_article']			= 'Блог по теме';
$_['text_author_information']		= '(автор)';
$_['text_posted_by']				= 'Сообщение от';
$_['text_on']						= 'включить';
$_['text_updated']					= 'Обновить до';
$_['text_created']					= 'Дата создания';
$_['text_comment_on_article']		= ' Комментарии к этой статье -';
$_['text_view_comment']				= ' Посмотреть комментарии';
$_['text_write_comment']			= 'Оставить комментарий';
$_['text_note']						= 'Обратите внимение: HTML не переводится!';
$_['text_comments']					= ' Комментарии';
$_['text_comment']					= ' Комментарий';
$_['text_no_blog']   				= 'К этому блогу нет комментариев.';
$_['text_on']           			= ' включить ';
$_['text_success']      			= 'Спасибо за Ваш комментарий!';
$_['text_success_approval']			= 'Спасибо за Ваш комментарий. Он отправлен администратору для подтверждения!';
$_['text_wait']						= 'Подождите!';
$_['text_reply_comment']			= 'Ответить';
$_['text_said']						= 'сказать:';
$_['text_authors']					= 'Авторы';
$_['text_pagination']				= 'Страница';
$_['text_latest_all']				= 'Последние посты';

$_['text_category_error']			= 'Категория блога не найдена!';
$_['text_author_error']				= 'Автор блога не найден!';
$_['text_article_error']			= 'Блог не найден!';

// Error
$_['error_name']        			= 'Внимание: Имя автора должно быть от 3 до 25 символов!';
$_['error_text']        			= 'Внимание: Текст комментария должен быть от 3 до 1000 символов!';
$_['error_captcha']     			= 'Внимание: Код подтверждения не соответствует изображению!';
$_['text_no_database'] = 'Отсутствуют таблицы базы данных для этого расширения, пожалуйста, установите "Simple Blog"!';


$_['button_continue'] = 'Продолжить';
$_['text_sidebar'] = 'Боковая панель';