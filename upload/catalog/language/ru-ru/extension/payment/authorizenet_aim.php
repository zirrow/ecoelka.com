<?php
// Text
$_['text_title']           = 'Кредитная карта / Дебетовая карта (Authorize.Net)';
$_['text_credit_card']     = 'Детали кредитной карты';

// Entry
$_['entry_cc_owner']       = 'Владелец карты';
$_['entry_cc_number']      = 'Номер карты';
$_['entry_cc_expire_date'] = 'Срок действия карты';
$_['entry_cc_cvv2']        = 'Защитный код карты (CVV2)';