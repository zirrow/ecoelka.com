<?php
// Text
$_['text_total_shipping']		= 'Доставка';
$_['text_total_discount']		= 'Скидка';
$_['text_total_tax']			= 'Оплата';
$_['text_total_sub']			= 'Промежуточная сумма';
$_['text_total']				= 'Общая сумма';
$_['text_smp_id']				= 'ID менеджера по продажам: ';
$_['text_buyer']				= 'Имя пользователя покупателя: ';