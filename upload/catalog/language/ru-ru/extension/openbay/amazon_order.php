<?php
// Text
$_['text_paid_amazon'] 			= 'Оплатить на Amazon';
$_['text_total_shipping'] 		= 'Доставка';
$_['text_total_shipping_tax'] 	= 'Оплата за доставку';
$_['text_total_giftwrap'] 		= 'Подарочная упаковка';
$_['text_total_giftwrap_tax'] 	= 'Оплата за подарочную упаковку';
$_['text_total_sub'] 			= 'Промежуточная сумма';
$_['text_tax'] 					= 'Оплата';
$_['text_total'] 				= 'Общая суммма';
$_['text_gift_message'] 		= 'Поздравительное сообщение';