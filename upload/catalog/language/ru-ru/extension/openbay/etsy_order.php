<?php
// Text
$_['text_total_shipping'] = 'Доставка';
$_['text_total_discount'] = 'Скидка';
$_['text_total_tax']      = 'Оплата';
$_['text_total_sub']      = 'Промежуточная сумма';
$_['text_total']          = 'Итого';