<?php
class ControllerExtensionModuleSohtmlcontent extends Controller
{
	/**
	 * @param $setting
	 * @return string
	 */
	public function index($setting)
	{
		$module_id = (isset($setting['moduleid']) && $setting['moduleid']) ? $setting['moduleid'] : 0;

		if($setting['use_cache']) {

			$cache_key = 'so_modules'
				. CACHE_DELIMITER . 'html_content'
				. CACHE_DELIMITER . $this->config->get('config_store_id')
				. CACHE_DELIMITER . $this->config->get('config_language_id')
				. CACHE_DELIMITER . $module_id;

			$html = $this->cache->get($cache_key);

			if(false === $html || null === $html){

				$html = $this->getContent($setting);

				$this->cache->set($cache_key, $html);

				if(CACHE_ADAPTER === 'redis' && isset($setting['cache_time']) && (int)$setting['cache_time'] > 0){
					$this->cache->expire($cache_key, (int)$setting['cache_time']);
				}
			}

		} else {

			$html = $this->getContent($setting);
		}

		return $html;
	}

	/**
	 * @param $setting
	 * @return string
	 */
	private function getContent($setting)
	{
		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {

			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
			$data['class_suffix'] 			= $setting['class_suffix'];
			$data['store_layout'] 			= $setting['store_layout'];

			return $this->load->view('extension/module/so_html_content/'. $setting['store_layout'], $data);
		}

		return '';
	}
}
