<?php
// Heading
$_['heading_title']    		= 'So Галлерея Instagram';
$_['heading_title_so']    	= $_['heading_title'].' <p class="btn btn-info btn-xs">версия 1.2.1</p>';
$_['entry_button_clear_cache']  = 'Сбросить кеш';
// Text
$_['text_module']               = 'Модули';
$_['text_success']              = 'Готово: Вы изменили встроенный модуль!';
$_['text_success_remove']       = 'Готово: Кеш сброшен успешно!';
$_['text_edit']                 = 'Редактировать So модуль галереи Instagram';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактировать модуль ';
$_['text_layout']      			= 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и Редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и Создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';

// value
$_['value_simple']			= 'Простой';
$_['value_slider']			= 'Слайдер';
// Entry
$_['entry_name']       					= 'Название модуля';
$_['entry_name_desc']       			= 'Модуль должен иметь название.';
$_['entry_head_name']         			= 'Заголовок верхнего колонтитула';
$_['entry_head_name_desc']    			= 'Озаглавить верхний колонтитул .';
$_['entry_display_title_module']       	= 'Показать заголовок модуля';
$_['entry_display_title_module_desc']   = 'Показать заголовок модуля.';
$_['entry_status']     					= 'Статус';
$_['entry_status_desc']     			= 'Опубликовать / Отменить публикацию модуля';

// Options
$_['entry_module']     					= 'Общие настройки';
$_['entry_source_option']   			= 'Исходные настройки';
$_['entry_effect_option']    			= 'Настройки эффектов';
$_['entry_advanced_option']     		= 'Рекомендуемые настройки';

// General Options
$_['entry_class_suffix']  			= 'Модуль суффикс Class';
$_['entry_class_suffix_desc']		= 'Суффикс Class для модуля.';
$_['entry_type_show']  				= 'Тип показа';
$_['entry_type_show_desc']  		= 'Тип показывается когда вы нажимаете на тип показа';
$_['entry_row']     				= '# Ряды';
$_['entry_row_desc']     			= 'Отображение строки конфигурации';
$_['entry_column']     		        = '# Колонки';
$_['entry_column0']     		        = 'Колонки - Устройства с большим экраном';
$_['entry_column1']     		        = 'Колонки - Компьютеры и ноутбуки';
$_['entry_column2']     		        = 'Колонки - Планшеты';
$_['entry_column3']     		        = 'Колонки - Мобильные телефоны в полноэкранном режиме';
$_['entry_column4']     		        = 'Колонки - Мобильные телефоны в портретном режиме';
$_['entry_nb_column0_desc'] 		= 'Для устройств с шириной экрана >= 1200px и больше.';
$_['entry_nb_column1_desc'] 		= 'Для устройств с шириной экрана от >= 992px до < 1200px.';
$_['entry_nb_column2_desc'] 		= 'Для устройств с шириной экрана от >= 768px до < 992px.';
$_['entry_nb_column3_desc'] 		= 'Для устройств с шириной экрана от >= 480px до < 768px.';
$_['entry_nb_column4_desc'] 		= 'Для устройств с шириной экрана от < 480px.';

// Source Options
$_['entry_users_id']       				= 'ID пользователей Instagram';
$_['entry_users_id_desc']       		= 'ID пользователей Instagram.';
$_['entry_access_token']       			= 'Маркер доступа';
$_['entry_access_token_desc']       	= 'Маркер доступа.';
$_['entry_show_fullname']       		= 'Показать полное имя';
$_['entry_show_fullname_desc']       	= 'Показать полное имя.';
$_['entry_limit_image']       			= 'Ограничить изображения';
$_['entry_limit_image_desc']       		= 'Ограничить изображения.';

// Effect
$_['effect_none'] 						= 'Нет';
$_['effect_fadeIn'] 					= 'Ослабить';
$_['effect_zoomIn'] 					= 'Приблизить';
$_['effect_zoomOut'] 					= 'Отдалить';
$_['effect_slideLeft'] 					= 'Влево';
$_['effect_slideRight'] 				= 'Вправо';
$_['effect_slideTop'] 					= 'Вверх';
$_['effect_slideBottom'] 				= 'Вниз';
$_['effect_flip'] 						= 'Повернуть';
$_['effect_flipInX'] 					= 'Повернуть по горизонтали';
$_['effect_flipInY'] 					= 'Повернуть по вертикали';
$_['effect_starwars'] 					= 'Star war';
$_['effect_bounceIn'] 					= 'Перетащить';
$_['effect_bounceInUp'] 				= 'Перетащить вверх';
$_['effect_bounceInDown'] 				= 'Перетащить вниз';
$_['effect_pageTop'] 					= 'Вверх страницы';
$_['effect_pageBottom'] 				= 'Повернуть';

$_['entry_margin']                		= 'Отступ вправо';
$_['entry_margin_desc']             	= 'Отступ вправо (px) между продуктами.';
$_['entry_slideBy']               		= 'Перелистывать по товарам';
$_['entry_slideBy_desc']            	= 'Навигация слайда на x. строке страницы может быть установлена для перехода по страницам.';
$_['entry_autoplay']	                = 'Автовоспроизведение';
$_['entry_autoplaySpeed']    			= 'Скорость автовоспроизведения';
$_['entry_autoplaySpeed_desc']    		= 'Скорость автовоспроизведения.';
$_['entry_smartSpeed']    				= 'Умная скорость';
$_['entry_smartSpeed_desc']    			= 'Автоматическая скорость автовоспроизведения.';
$_['entry_startPosition']    			= 'Начальная позиция';
$_['entry_startPosition_desc']    		= 'Начальная позиция или URL хэш-строки как #id.';
$_['entry_mouseDrag']    				= 'Перетаскивание мышью';
$_['entry_mouseDrag_desc']    			= 'Перетаскивание мышью включено';
$_['entry_touchDrag']    				= 'Touch перетаскивание';
$_['entry_touchDrag_desc']    			= 'Touch перетаскивание включено';
$_['entry_dots']    					= 'Показать разбивку на страницы';
$_['entry_dots_desc']    				= 'Разрешить отображение / скрытие разбивки на страницы для модуля';
$_['entry_dotsSpeed']    				= 'Скорость разбивки на страницы';
$_['entry_dotsSpeed_desc']    			= 'Скорость разбивки на страницы.';
$_['entry_button_page']  		    	= 'Страница кнопок';
$_['entry_button_page_desc']        	= 'Отображение типа кнопки на странице кнопок';
$_['entry_navs']    					= 'Показать навигацию';
$_['entry_navs_desc']    				= 'Разрешить отображение / скрытие навигации для модуля';
$_['entry_navspeed']    				= 'Скорость навигации';
$_['entry_navspeed_desc']    			= 'Скорость навигации.';
$_['entry_display_nav']	                = 'Отобрзить навигацию';
$_['entry_effect']	                    = 'Эффект';
$_['entry_loop']	                    = 'Петля';
$_['entry_loop_desc']	                = 'Отобрзить петлю';
$_['entry_duration']              		= 'Продолжительность';
$_['entry_duration_desc']             	= 'Определение продолжительности анимации. Больше = медленнее.';
$_['entry_delay']                 		= 'Задержка';
$_['entry_delay_desc']                 	= 'Устанавливить таймер для задержки выполнения следующего элемента в очереди. Больше = медленнее.';
$_['entry_pausehover']                  = 'Пауза при наведении';
$_['entry_pausehover_desc']             = 'Пауза при наведении мыши';
$_['entry_autoplay_timeout']            = 'Автоматический интервал ожидания';
$_['entry_autoplay_timeout_desc']       = 'Тайм-аут интервала автовоспроизведения для слайдера.';

//Tabs Advanced 
$_['entry_pre_text']                	= 'Пред-текст';
$_['entry_pre_text_desc']               = 'Верхний колонтитул модуля';
$_['entry_post_text']               	= 'Пост-текст';
$_['entry_post_text_desc']              = 'Нижний колонтитул модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля ';
$_['entry_cache_time']                  = 'Время кеширования';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед повторным кешированием модуля.';

// Help
$_['help_product']     					= '(Автозаполнение)';

// Error
$_['error_warning']          	= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			= 'Внимание: У вас нет разрешения на изменение встроенного модуля!';
$_['error_name']       			= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	= 'Имя заголовка модуля должно быть от 3 до 64 символов!';
$_['error_users_id']       		= 'The string character format is required in this user id!';
$_['error_access_token']       	= 'The string character format is required in this access token!';
$_['error_limit_image'] 		= 'The number character format is required in this limit image!';
$_['error_margin'] 	            = 'The number character format is required in this margin!';
$_['error_slideBy'] 	            = 'The number character format is required in this slideBy!';
$_['error_autoplay_timeout'] 	= 'The number character format is required in this auto play timeout!';
$_['error_autoplaySpeed'] 	= 'The number character format is required in this auto play speed!';
$_['error_smartSpeed'] 	= 'The number character format is required in this smart speed!';
$_['error_startPosition'] 	= 'The number character format is required in this start position!';
$_['error_navSpeed'] 	= 'The number character format is required in this nav speed!';
$_['error_duration'] 	= 'The number character format is required in this duration!';
$_['error_delay'] 	= 'The number character format is required in this delay!';
$_['error_dotsSpeed'] 	= 'The number character format is required in this pagination!';
