﻿<?php
// Heading
$_['heading_title']    		    = 'So Html Контент';
$_['heading_title_so']    	    = $_['heading_title'].' <p class="btn btn-info btn-xs">версия 1.2.0</p>';

$_['entry_button_clear_cache']  = 'Сбросить кеш';
// Text
$_['text_module']               = 'Модули';
$_['text_success']              = 'Успех: Вы успешно модифицировали модуль!';
$_['text_success_remove']       = 'Успех: Кешь сброшен успешно!';
$_['text_edit']                 = 'Редактировать модуля So Html Контент ';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактировать модуль ';
$_['text_layout']      			= 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';

// Entry
$_['entry_name']                    = 'Имя модуля';
$_['entry_title']                   = 'Заголовок';
$_['entry_description']             = 'Описание';
$_['entry_status']                  = 'Статус';
$_['entry_class_suffix']  			= 'Суффикс класса';
$_['entry_class_suffix_desc']		= 'Суффикс класса для модуля.';
$_['entry_use_cache']               = 'Кеширование';
$_['entry_use_cache_desc']          = 'Выберите, следует ли кэшировать содержимое этого модуля ';
$_['entry_cache_time']              = 'Время кеширования';
$_['entry_cache_time_desc']         = 'Время (в секундах) перед повторным кешированием.';
// Help
$_['help_product']     				= '(Автозаполнение)';

// Error
$_['error_warning']          	    = 'Внимание! Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			    = 'Внимание: у вас нет разрешения на изменение модуля!';
$_['error_name']       			    = 'Имя модуля должно быть от 3 до 64 символов!';

// value
$_['entry_store_layout']            = 'Макет магазина';
$_['entry_store_layout_desc']       = 'Выбрать макет показа магазина';
$_['value_default']   		        = 'По умолчанию';
$_['value_default2']   		        = 'По умолчанию 2';
$_['value_default3']   		        = 'По умолчанию 3';
$_['value_default4']   		        = 'По умолчанию 4';