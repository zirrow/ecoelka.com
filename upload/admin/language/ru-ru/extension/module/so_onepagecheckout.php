<?php
// Heading
$_['heading_title']			= 'So Одностраничная корзина';
$_['heading_title_so']    	= 'So Одностраничная корзина <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 1.1.2</b>';

// Text
$_['text_module']      					= 'Модуль';
$_['text_extension']   					= 'Extensions';
$_['text_success']     					= 'Готово: Вы изменили So модуль Одностраничная корзина!';
$_['text_edit']        					= 'Реедактировать So модуль Одностраничная корзина';
$_['text_default_country']        		= 'Страна по умолчанию';
$_['text_default_zone']        			= 'Область по умолчанию';
$_['text_register_account']        		= 'Зарегистрировать аккаунт';
$_['text_guest_checkout']        		= 'Проверка гостей';
$_['text_login_checkout']        		= 'Проверка входа';
$_['text_default_display']        		= 'Отображение по умолчанию';
$_['text_register']        				= 'Регистрация';
$_['text_account_setting'] 				= 'Настройки аккаунта';
$_['text_guest']        				= 'Гость';
$_['text_login']        				= 'Вход';
$_['text_shopping_cart']        		= 'Корзина';
$_['text_shopping_cart_status']        	= 'Статус корзины';
$_['text_show_weight']        			= 'Показать вес';
$_['text_quantity_update_permission']	= 'Показать количество обновлений';
$_['text_show_removecart']				= 'Показать удаленную корзину';
$_['text_product_image_size']			= 'Размер изображения продукта (Ширина/Высота)';
$_['text_delivery_methods']				= 'Способ доставки';
$_['text_delivery_methods_status']		= 'Статус способа доставки';
$_['text_payment_methods_status']		= 'Статус способа оплаты';
$_['text_payment_methods']				= 'Способы оплаты';
$_['text_confirm_order']				= 'Подтвердить заказ';
$_['text_add_comments']					= 'Добавить комментарии';
$_['text_require_comment']				= 'Требовать комментарий';
$_['text_show_newsletter']				= 'Показать новостную рассылку';
$_['text_show_privacy']					= 'Показать приватность';
$_['text_show_term']					= 'Показать сроки и условия';
$_['text_checkout_order_buttons']		= 'Кнопка Проверить заказ';
$_['text_coupon_voucher']				= 'Купон и Ваучер';
$_['text_show_module_name']				= 'Показать название модуля';
$_['text_coupon']						= 'Купон';
$_['text_reward']						= 'Вознаграждение';
$_['text_voucher']						= 'Ваучер';
$_['text_login_account']				= 'Постоянный клиент';
$_['text_status']						= 'Статус';
$_['text_layout']						= 'Размещение';
$_['text_layout_one']					= 'Размещение 1';
$_['text_layout_two']					= 'Размещение 2';
$_['text_layout_three']					= 'Размещение 3';
$_['text_layout_title']					= 'Установить стиль макета оформления заказа. Доступно в одном, двух или трех столбцах.';
$_['text_shipping_methods']				= 'Способы доставки';

// Entry
$_['entry_name']       		= 'Название модуля';
$_['entry_status']     		= 'Статус';
$_['tab_general']      		= 'Общие';
$_['tab_account_setting']	= 'Настройки аккаунта';
$_['tab_layout_setting']	= 'Настройки размещения';
$_['tab_shipping_cart']		= 'Корзина';
$_['tab_delivery_methods']	= 'Способы доставки';
$_['tab_payment_methods']	= 'Способы оплаты';
$_['tab_confirm_order']		= 'Подтвердить заказ';
$_['tab_help']     			= 'Помощь!';

$_['entry_name_title']     			= 'Название модуля';
$_['entry_status_title']     		= 'Включить или отключить модуль';

// Error
$_['error_permission'] 		= 'Внимание: У вас нет разрешения на изменение So модуля Одностраничная корзина!';
$_['error_name']       		= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_warning']			= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_product_image_width']			= 'Ширина изображения продукта должна быть числом';
$_['error_product_image_height']			= 'Высота изображения продукта должна быть числом';

$_['entry_button_save_and_edit']	= 'Сохранить и Остаться';
$_['entry_button_save_and_new']		= 'Сохранить и Создать новый';