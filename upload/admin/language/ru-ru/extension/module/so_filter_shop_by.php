<?php
// Heading
$_['heading_title']    		= 'So Фильтровать магазин по';
$_['heading_title_so']    	= 'So Фильтровать магазин по <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 2.2.0</b>';
$_['entry_button_clear_cache']  = 'Сбросить кеш';
// Text
$_['text_module']               = 'Модули';
$_['text_success']              = 'Готово: Вы изменили встроенный модуль!';
$_['text_success_remove']       = 'Готово: Кэш сброшен успешно!';
$_['text_edit']                 = 'Редактировать So модуль Фильтровать магазина по';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактировать модуль ';
$_['text_layout']      			= 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и Редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и Создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';

// Entry
$_['entry_name']       					= 'Название модуля';
$_['entry_name_desc']       			= 'Модуль долен иметь имя.';
$_['entry_head_name']         			= 'Заголовок верхнего колонтитула';
$_['entry_head_name_desc']    			= 'Дать название заголовку верхнего колонтитула.';
$_['entry_display_title_module']       	= 'Показать заголовок модуля';
$_['entry_display_title_module_desc']   = 'Показать заголовок модуля.';
$_['entry_status']     					= 'Статус';
$_['entry_status_desc']     			= 'Опубликовать / Отменить публикацию модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кешировать содержимое этого модуля ';
$_['entry_cache_time']                  = 'Время кеширования';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед рекешем модуля.';

// Options
$_['entry_module']     					= 'Общие настройки';
$_['entry_base_option']   				= 'Исходные настройки';
$_['entry_attribute_option']  			= 'Свойства';
$_['entry_option_option']    			= 'Настройки';

// General Options
$_['entry_class_suffix']  		= 'Суффикс класса модуля';
$_['entry_class_suffix_desc']	= 'Суффикс класса для модуля.';
$_['entry_in_class']  			= 'В классе';
$_['entry_in_class_desc']		= 'В классе.';

// Base Options
$_['entry_manufacturer']  			= 'Производитель';
$_['entry_manufacturer_desc']		= 'Производитель.';
$_['entry_pro_price']  				= 'Отображать стоимость товара';
$_['entry_pro_price_desc']			= 'Отображать стоимость товара.';
$_['entry_search_text']  			= 'Отображать поиск текста';
$_['entry_search_text_desc']		= 'Отображать поиск текста.';
$_['entry_character_search']  		= 'Поиск характеристик';
$_['entry_character_search_desc']	= 'Поиск характеристик.';
$_['entry_rating']  				= 'Отображать рейтинг';
$_['entry_rating_desc']				= 'Отображать рейтинг.';
$_['entry_reset_all']  				= 'Отображать Сбросить все';
$_['entry_reset_all_desc']			= 'Отображать Сбросить все.';
$_['entry_subcategory']  			= 'Отображать подкатегории';
$_['entry_subcategory_desc']		= 'Отображать подкатегории.';
// Options 
$_['entry_display_option_radio']  			= 'Радио';
$_['entry_display_option_radio_desc']		= 'Показать настройки радио.';
$_['entry_display_option_checkbox']  		= 'Флажок';
$_['entry_display_option_checkbox_desc']	= 'Показать настройки флажка.';
$_['entry_display_option_select']  			= 'Подбор';
$_['entry_display_option_select_desc']		= 'Показать настройки подбора.';
$_['entry_display_option_size']  			= 'Размер';
$_['entry_display_option_size_desc']		= 'Показать настройки размера.';
$_['entry_display_option_color']  			= 'Цвет';
$_['entry_display_option_color_desc']		= 'Показать настройки цвета.';
 
// Help
$_['help_product']     					= '(Автозаполнение)';

// Error
$_['error_warning']          	= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			= 'Внимание: У вас нет разрешения на изменение модуля!';
$_['error_name']       			= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	= 'Название заголовка модуля должно быть от 3 до 64 символов!';
$_['error_character_search']  	= 'В поиске символов требуется формат номера символов!';
