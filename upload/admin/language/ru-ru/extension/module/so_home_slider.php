<?php
// Heading
$_['heading_title']    = 'So Home Slider';
$_['heading_title_so']    	= $_['heading_title'].' <p class="btn btn-info btn-xs">версия 2.2.0</p>';
$_['entry_button_clear_cache']  = 'Сбросить кеш';
// Text
$_['text_module']      			= 'Модули';
$_['text_success']     			= 'Готово: Вы изменили встроенный модуль!';
$_['text_success_remove']       = 'Готово: Кеш сброшен успешно!';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактировать модуль ';
$_['text_layout']      			= 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и Редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и Создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';

//Group
$_['tab_manage_slider_group']       = 'Управление ползунком';
$_['tab_manage_module']             = 'Управление модулем';
$_['entry_status_slide_desc']     	= 'Опубликовать / Отменить публикацию ползунка';
$_['entry_image']     				= 'Изображение ползунка';
$_['entry_image_desc']     			= 'Изображение ползунка.';
//Manager
$_['entry_id']       					= 'ID: ';
$_['entry_id_desc']       				= 'Id группы.';
$_['entry_title_group']       			= 'Название группы';
$_['entry_title_group_desc']       		= 'Название группы.';
$_['entry_animateOut']     				= 'Выберите эффект анимации Out';
$_['entry_animateOut_desc']    			= 'Выберите анимацию Out для модуля здесь.';
$_['entry_animateIn']     				= 'Выберите эффект анимации In ';
$_['entry_animateIn_desc']    			= 'Выберите анимацию In для модуля здесь.';
// value
$_['value_blank']			= 'Новое окно';
$_['value_self']			= 'Такое же окно';
$_['value_name']       		= 'Название';
$_['value_model']       	= 'Модель';
$_['value_price']       	= 'Цена';
$_['value_quantity']       	= 'Количество';
$_['value_rating']       	= 'Рейтинг';
$_['value_sort_order']      = 'Порядок сортировки';
$_['value_date_added']      = 'Дата добавления';
$_['value_sales']       	= 'Распрродажа';
$_['value_asc']       		= 'Поднимающийся';
$_['value_desc']       		= 'Опускающийся';
$_['value_top']       		= 'Верх';
$_['value_under']       	= 'Низ';
//Default
$_['entry_include']     = 'Включить';
$_['entry_exclude']     = 'Исключить';

// Entry
$_['entry_name']              			= 'Название модуля';
$_['entry_name_desc']         			= 'У модуля должно быть название';
$_['entry_head_name']         			= 'Заголовок верхнего колонтитула';
$_['entry_head_name_desc']    			= 'Заголовок верхнего колонтитула';
$_['entry_display_title_module']       	= 'Показать заголовок верхнего колонтитула';
$_['entry_display_title_module_desc']   = 'Показать название модуля.';
$_['entry_status_desc']     			= 'Опубликовать / Отменить публикацию модуля';

// Tabs
$_['entry_module']            = 'Настройки модуля';
$_['entry_slider_option']     = 'Настройки слайдера';
$_['entry_tabs_option']       = 'Настройки вкладок';
$_['entry_advanced_option']   = 'Рекомендуемые настройки';
$_['entry_image_option']      = 'Настройки изображений';
$_['entry_effect_option']     = 'Настройки эффектов';

//Tabs General
$_['entry_class_suffix']  		    = 'Суффикс класса модуля';
$_['entry_class_suffix_desc']  		= 'Суффикс класса для модуля.';
$_['entry_open_link']  				= 'Открыть ссылку';
$_['entry_open_link_desc']  		= 'Тип отображается, когда вы нажимаете на ссылку';
$_['entry_column']     		        = '# Колонки';
$_['entry_nb_row']                  = '# Ряды';
$_['entry_nb_row_desc']             = 'Количество рядов ';
$_['entry_nb_column0_desc']         = 'Для устройств с шириной экрана от >= 1200px до greater.';
$_['entry_nb_column1_desc']         = 'Для устройств с шириной экрана от >= 992px до < 1200px.';
$_['entry_nb_column2_desc']         = 'Для устройств с шириной экрана от >= 768px до < 992px.';
$_['entry_nb_column3_desc']         = 'Для устройств с шириной экрана от >= 480px до < 768px ';
$_['entry_nb_column4_desc']         = 'Для устройств с шириной экрана от < 480px ';
$_['entry_width'] 						= 'Ширина';
$_['entry_width_desc'] 					= 'Ширина изображения';
$_['entry_height'] 						= 'Высотаt';
$_['entry_height_desc'] 				= 'Высота изображения';
$_['entry_slide_title']     		= 'Название слайда';
$_['entry_slide_title_desc']     	= 'Название слайда';
$_['entry_link']     				= 'Ссылка';
$_['entry_link_desc']     			= 'Ссылка';

$_['entry_caption']     				= 'Заголовок';
$_['entry_caption_desc']     			= 'Заголовок';
$_['entry_description']     			= 'Описание';
$_['entry_description_desc']     		= 'Описание';

$_['label_listslide'] 					= 'Список слайдов';
//Tabs Effect options
$_['effect_none'] 						= 'Нет';
$_['effect_fadeIn'] 					= 'Постепенное появление';
$_['effect_zoomIn'] 					= 'Приблизить';
$_['effect_zoomOut'] 					= 'Отдалить';
$_['effect_slideLeft'] 					= 'Влево';
$_['effect_slideRight'] 				= 'Вправо';
$_['effect_slideTop'] 					= 'Вверх';
$_['effect_slideBottom'] 				= 'Вниз';
$_['effect_flip'] 						= 'Повернуть';
$_['effect_flipInX'] 					= 'Повернуть по горизонтали';
$_['effect_flipInY'] 					= 'Повернуть по вертикали';
$_['effect_starwars'] 					= 'Star war';
$_['effect_bounceIn'] 					= 'Перетащить';
$_['effect_bounceInUp'] 				= 'Перетащить вверх';
$_['effect_bounceInDown'] 				= 'Перетащить вниз';
$_['effect_pageTop'] 					= 'Вверх страницы';
$_['effect_pageBottom'] 				= 'Повернуть';

$_['entry_margin']                		= 'Отступ в право';
$_['entry_margin_desc']             	= 'Отступ в право (px) между продуктами.';
$_['entry_slideBy']               		= 'Перелистывать по товарам';
$_['entry_slideBy_desc']            	= 'Навигация перелистывания x. строк на странице может быть установлена для перехода по страницам.';
$_['entry_autoplay']              		= 'Автовоспроизведение
';
$_['entry_autoplay_desc']           	= 'Разрешить автоматическое воспроизведение вкл / выкл для слайдера';
$_['entry_autoplayTimeout']      		= 'Автоматический интервал ожидания';
$_['entry_autoplayTimeout_desc']    	= 'Тайм-аут интервала автовоспроизведения для слайдера.';
$_['entry_autoplayHoverPause']    		= 'Пауза при наведении';
$_['entry_autoplayHoverPause_desc']    	= 'Пауза при наведении мыши';
$_['entry_autoplaySpeed']    			= 'Скорость автовоспроизведения';
$_['entry_autoplaySpeed_desc']    		= 'Скорость автовоспроизведения.';
$_['entry_smartSpeed']    				= 'Умная скорость';
$_['entry_smartSpeed_desc']    			= 'Автоматическая скорость автовоспроизведения.';
$_['entry_startPosition']    			= 'Начальная позиция товара';
$_['entry_startPosition_desc']    		= 'Начальная позиция или строка хеша URL, например #id.';
$_['entry_mouseDrag']    				= 'Перетаскивание мышью';
$_['entry_mouseDrag_desc']    			= 'Перетаскивание мышью включено';
$_['entry_touchDrag']    				= 'Touch перетаскивание';
$_['entry_touchDrag_desc']    			= 'Touch перетаскивание включено';
$_['entry_loop']    				    = 'Петля';
$_['entry_loop_desc']    			    = 'Петля включена';
$_['entry_dots']    					= 'Показать разбивку на страницы';
$_['entry_dots_desc']    				= 'Разрешить отображение / скрытие разбивки на страницы для модуля';
$_['entry_navs']    					= 'Показать навигацию';
$_['entry_navs_desc']    				= 'Разрешить отображение / скрытие навигации для модуля';
$_['entry_button_page']    				= 'Страница кнопок';
$_['entry_button_page_desc']    		= 'Страница кнопок';
$_['entry_dotsSpeed']    				= 'Скорость разбивки на страницы';
$_['entry_dotsSpeed_desc']    			= 'Скорость разбивки на страницы.';
$_['entry_navspeed']    				= 'Скорость навигации';
$_['entry_navspeed_desc']    			= 'Скорость навигации.';
$_['entry_effect']                		= 'Выберите эффект';
$_['entry_effect_desc']                	= 'Выберите эффект для модуля здесь.';
$_['entry_duration']              		= 'Продолжительность';
$_['entry_duration_desc']              	= 'Определение продолжительности анимации. Больше = медленнее.';
$_['entry_delay']                 		= 'Отсрочка';
$_['entry_delay_desc']                 	= 'Устанавливает таймер для задержки выполнения следующего элемента в очереди. Больше = медленнее.';

//Tabs Advanced 
$_['entry_pre_text']                	= 'Верхний колонтитул';
$_['entry_pre_text_desc']               = 'Вводный текст модуля';
$_['entry_post_text']               	= 'Нижний колонтитул';
$_['entry_post_text_desc']              = 'Нижний колонтитул модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кешировать содержимое этого модуля ';
$_['entry_cache_time']                  = 'Время кеширования';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед рекешем модуля.';

// Help
$_['help_product']     = '(Автозаполнение)';
$_['entry_status']     = 'Статус';
// Error
$_['error_warning']          	= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			= 'Внимание: У вас нет разрешения на изменение модуля So Home Slider!';
$_['error_name']       			= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	= 'Название заголовка модуля должно быть от 3 до 64 символов!';
$_['error_category']   			= 'This field must not be empty!';
$_['error_category_depth']  	= 'The number character format is required in this category depth!';
$_['error_source_limit'] 		= 'The number character format is required in this limitation!';
$_['error_title_maxlength'] 	= 'The number character format is required in this title maxlength!';
$_['error_description_maxlength'] = 'The number character format is required in this description maxlength!';
$_['error_width']      			= 'The number character format from 1 to 5000 is required in this width!';
$_['error_height']     			= 'The number character format from 1 to 5000 is required in this height!';
$_['error_placeholder_path']    = 'The placeholder path must enter the picture, eg: nophoto.png';
$_['error_margin']     			= 'The number character format is required in this margin!';
$_['error_slideBy']     		= 'The number character format is required in this slide by!';
$_['error_smartSpeed']     		= 'The number character format is required in this smart speed!';
$_['error_startPosition']     	= 'The number character format is required in this start position!';
$_['error_dotsSpeed']     		= 'The number character format is required in this pagination speed!';
$_['error_navSpeed']     		= 'The number character format is required in this navigation speed!';
$_['error_duration']     		= 'The number character format is required in this duration!';
$_['error_delay']     			= 'The number character format is required in this delay!';
$_['error_autoplayTimeout']     = 'The number character format is required in this Autoplay Timeout!';
$_['error_autoplaySpeed']     	= 'The number character format is required in this Autoplay Speed!';
$_['error_image']   			= 'Это поле не должно быть пустым!';
$_['error_slide_title']   		= 'Это поле не должно быть пустым!';


