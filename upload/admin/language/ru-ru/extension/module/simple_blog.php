<?php
// Heading
$_['heading_title']       		= 'Simple Blog';
$_['simple_blog_seo_keyword'] 	= 'Simple Blog Seo Keyword Heading:';
$_['simple_blog_heading']      	= 'Simple Blog Title:';
$_['blog_module_heading']		= 'Заголовок модуля:';
$_['product_related_heading']   = 'Заголовок связанных продуктов:';
$_['comment_related_heading']   = 'Заголовок связанных коментариев:';

$_['entry_simple_blog_image_width']                = 'Ширина картинки в последних статьях:';
$_['entry_simple_blog_image_height']               = 'Высота картинки в последних статьях:';
$_['entry_simple_blog_short_description_length']   = 'Длина краткого описания в последних статьях:';

// Help
$_['help_simple_blog_heading']          = 'Это имя будет отображаться в нижнем колонтитуле для перенаправления на блог, по умолчанию - Blog.';
$_['help_simple_blog_seo_keyword']      = 'Это имя будет как seo keyword когда simple blog будет нажат, по умолчанию - simple-blog.';
$_['help_blog_module_heading']          = 'Вы можете настроить заголовок блога, который показывает магазин, по умолчанию показывает Blogs.';
$_['help_product_related_heading']      = 'Вы можете установить заголовок который будет показываться для модуля "Связанные товары". По умолчанию отображается - Blog Related Products.';
$_['help_comment_related_heading']      = 'Вы можете настроить заголовок  который будет показываться для "связанных коментариев". По умолчанию - Blog Related Comments.';
$_['help_set_tagline']                  = 'Вы можете установить tagline, который отображается в разделе заголовка.';
$_['help_image']                        = 'Вы можете установить изображение для раздела заголовка блога.';
$_['help_display_category']             = 'Если отключить, будет отображаться навигация по категориям которая в opencart по умолчанию.';
$_['help_comment_approval']             = 'Если Да, комментарии для блога будут публиковаться без проверки.';
$_['help_author_information']           = 'Если включить, будет отображаться информация, связанная с автором.';
$_['help_related_article']              = 'Если включить, Связанная статья будет отображаться.';
$_['help_show_social_site_option']      = 'Если включить, будут отображаться столбцы блога.';
$_['help_show_author']                  = 'Если Да, Список авторов будет отображаться на странице автора.';

// Text
$_['text_extension']            = 'Расширения';
$_['text_success']        		= 'Success: You have modified module simple blog!';
$_['text_set_header']			= 'Назначить Header';
$_['text_set_footer']			= 'Назначить Footer';
$_['text_browse']				= 'Просмотреть';
$_['text_clear']				= 'Очистить';
$_['text_image_manager']		= 'Менеджер картинок';
$_['text_article_limit']		= 'Лимит статей';
$_['text_category']				= 'Категории';
$_['text_content_top']    		= 'Вверху';
$_['text_content_bottom'] 		= 'Внизу';
$_['text_column_left']    		= 'Слева';
$_['text_column_right']   		= 'Справа';
$_['text_category_label']		= 'Из всех категорий';
$_['text_latest_article']		= 'Самые свежие статьи';
$_['text_popular_article']		= 'Популярные статьи';
$_['text_article_related']		= 'Article Related Module';
$_['text_edit']		            = 'Редактировать Simple Blog';

// Entry
$_['entry_status']				= 'Статус:';
$_['entry_custom_theme']		= 'Use Custom Header: <br /><span class="help"> Use custom header for blog. if yes then you can set header.</span>';
$_['entry_set_tagline']			= 'Tag Line:';
$_['entry_image']				= 'Blog Header Image:';
$_['entry_display_category']	= 'Навигация по блогу в шапке:';
$_['entry_comment_approval']	= 'Авто публикация коментариев:';
$_['entry_author_information']	= 'Об авторе:';
$_['entry_article_limit']		= 'Лимит статей';
$_['entry_category']			= 'Категории';
$_['entry_layout']        		= 'Поля:';
$_['entry_position']      		= 'Позиция:';
$_['entry_sort_order']    		= 'ПОряок сортировки:';
$_['entry_related_article']    	= 'Связанные статьи:';
$_['entry_show_blog_column']    = 'Показать калонки блога ';
$_['entry_show_author'] 		= 'Показывать автора:';

// Error
$_['error_permission']    		= 'Warning: You do not have permission to modify module simple blog!';