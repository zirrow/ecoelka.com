<?php
// Heading
$_['heading_title']    = 'So Новостная рассылка Кастомная всплывашка';
$_['heading_title_so']    	= $_['heading_title'].' <p class="btn btn-info btn-xs">версия 3.0.0</p>';
$_['entry_button_clear_cache']  = 'Сбросить кеш';
// Text
$_['text_module']      			= 'Модули';
$_['text_success']     			= 'Готово: Вы изменили встроенный модуль!';
$_['text_success_remove']       = 'Готово: Кеш сброшен успешно!';
$_['text_edit']        			= 'Редактировать So модуль Популярные вкладки';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактировать модуль ';
$_['text_layout']      			= 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// value
$_['value_layout1']			= 'Стандартный по умолчанию';
$_['value_layout2']			= 'Всплывашка';

$_['signup_options']			= 'Настройки подписки';
$_['newletter_options']			= 'Настройки новых писем';
// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и Редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и Создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';

//Default
$_['entry_include']     = 'Добавить';
$_['entry_exclude']     = 'Убрать';

// Entry
$_['entry_name']              			= 'Название модуля';
$_['entry_name_desc']         			= 'Модуль должен иметь название';
$_['entry_head_name']         			= 'Заголовок верхнего колонтитула';
$_['entry_head_name_desc']    			= 'Озаглавить верхний колонтитул';
$_['entry_display_title_module']       	= 'Показать заголовок модуля';
$_['entry_display_title_module_desc']   = 'Показать заголовок модуля.';
$_['entry_status_desc']     			= 'Опубликовать / Отменить публикацию модуля';

// Tabs
$_['entry_module']                  = 'Общие настройки';
$_['entry_style_option']            = 'Настройки стиля';
$_['entry_content_option']          = 'Настройки контента';
$_['entry_newsletter_subscribers']  = 'Подписчики на новостную рассылку';
$_['entry_html_Email_template']     = 'Шаблон HTML сообщения';
$_['entry_advanced_option']     	= 'Рекомендуемые настройки';
$_['entry_type_footer']     		= 'Показать нижний колонтитул';


//Tabs General options
$_['entry_class_suffix']  		    = 'Модуль суфикс Class';
$_['entry_class_suffix_desc']  		= 'Суфикс Class для модуля.';
$_['entry_open_link']  				= 'Открыть ссылку';
$_['entry_open_link_desc']  		= 'Тип отображается, когда вы нажимаете на ссылку';
$_['entry_expired']                 = 'Время истечения срока действия cookie (день)';
$_['entry_expired_desc']            = 'Время истечения срока действия cookie';
$_['entry_layout']              	= 'Расположение';
$_['entry_layout_desc']             = 'Показать расположение модуля';
$_['entry_width']                   = 'Ширина всплывашки';
$_['entry_width_desc']              = 'Размер всплывашки';
$_['entry_width_note']              = 'Например: 50% или 450px';
$_['entry_image']                   = 'Фоновое изображение';
$_['entry_image_bg_display']        = 'Показать фоновое изображение';
$_['entry_image_bg_display_desc']   = 'Показать/скрыть фоновое изображение';
$_['entry_color_bg']                = 'Цвет фона';
$_['entry_color_bg_desc']           = 'Выбрать цвет фона';

// Content
$_['entry_title_display']  	        = 'Показать заголовок';
$_['entry_title_display_desc']  	= 'Показать/скрыть  заголовок';
$_['entry_title']  	                = 'Заголовок всплывашки';
$_['entry_title_desc']  	        = 'Оставьте поле пустым, если вы не хотите показывать заголовок ';
$_['entry_newsletter_promo']  	    = 'Промо-текст для новостной рассылки';
$_['entry_newsletter_promo_desc']  	= 'Промо-текст для новостной рассылки';

// Tabs newsletter_subscribers_option
$_['entry_button_delete']  	                = 'Удалить';
$_['entry_delete_all']  	            = 'Удалить все';
$_['entry_delete_selected']          = 'Удалить выбранное';
$_['entry_delete_all_not_approved']  = 'Удалить все не подтверждено';

$_['entry_button_approve']  	            = 'Подтвердить';
$_['entry_approve_selected']  	    = 'Подтвердить выбранное';
$_['entry_approve_all_not_approved'] = 'Подтвердить все не подтверждено';

$_['entry_button_mailing']  	                            = 'Отправка сообщений';
$_['entry_mailing_send_email_to_all']                = 'Отправить сообщение всем';
$_['entry_mailing_send_email_to_all_selected']  	    = 'Отправить сообщение всем выбранным';
$_['entry_mailing_send_email_to_all_not_notified']  	= 'Отправить сообщение всем не подтверждено';
$_['entry_mailing_send_email_to_all_approved_only']  = 'Отправить письмо только всем подтвержденным';
$_['entry_revert_yet_send']     = 'Отменить отправку';

$_['entry_column_email']  	    = 'Email подписчика';
$_['entry_column_date_added']   = 'Дата добавления';
$_['entry_column_status']       = 'Статус';
$_['entry_confirm_mail']        = 'Подтвердить почту';
$_['entry_column_action']  	    = 'Действие';
$_['entry_not_approved']  	    = 'НЕ ПОДТВЕРЖДЕНО';
$_['entry_approved']  	        = 'ПОДТВЕРЖДЕНО';
$_['entry_yet_send']  	        = 'Уже отправленные';
$_['entry_did_send']  	        = 'Отправленные';

$_['entry_text_no_results']  	= 'Нет объектов для показа';

// Tabs HTML Email Template
$_['entry_email_template_subject']  	= 'Название темы';
$_['entry_email_template_subject_desc'] = 'Название темы';
$_['entry_content_email']               = 'Содержание письма';
$_['entry_content_email_desc']  	    = 'Содержание письма';

//Tabs Advanced 
$_['entry_pre_text']                	= 'Пред-текст';
$_['entry_pre_text_desc']               = 'Верхний колонтитул модуля';
$_['entry_post_text']               	= 'Пост-текст';
$_['entry_post_text_desc']              = 'Нижний колонтитул модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля ';
$_['entry_cache_time']                  = 'Время кеширования';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед повторным кешированием модуля.';

// Help
$_['help_product']     = '(Автозаполнение)';
$_['entry_status']     = 'Статус';
// Error
$_['error_warning']          	= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			= 'Внимание: У вас нет разрешения на изменение so модуля сделок!';
$_['error_name']       			= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	= 'Имя заголовка модуля должно быть от 3 до 64 символов!';

$_['error_expired']     		= 'The number character format is required in this expired!';
$_['error_heading_height']     	= 'The number character format is required in this heading height!';
$_['error_title']     	        = 'The number character format is required in this title!';
$_['error_newsletter_promo']     	= 'The number character format is required in this newsletter promo!';
$_['error_email_template_subject']  = 'The number character format is required in this Email template subject';


