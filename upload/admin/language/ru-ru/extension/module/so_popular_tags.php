<?php
// Heading
$_['heading_title']    			= 'So Популярные вкладки';
$_['heading_title_so']    		= 'So Популярные вкладки <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">vверсия 2.2.1</b>';
$_['entry_button_clear_cache']  = 'Сбросить кеш';
// Text
$_['text_module']      			= 'Модули';
$_['text_success']     			= 'Готово: Вы изменили встроенный модуль!';
$_['text_success_remove']       = 'Готово: Кеш сброшен успешно!';
$_['text_edit']        			= 'Редактировать So модуль Популярные вкладки';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактировать модуль ';
$_['text_layout']      			= 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и Редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и Создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';
// value
$_['value_blank']			= 'Новое окно';
$_['value_self']			= 'Такое же окно';
$_['value_lighter']			= 'Светлее';
$_['value_normal']			= 'Нормальный';
$_['value_bold']			= 'Жирный';
$_['value_bolder']			= 'Жирнее';

// Entry
$_['entry_name']       					= 'Название модуля';
$_['entry_name_desc']       			= 'Модуль должен иметь название.';
$_['entry_head_name']         			= 'Заголовок верхнего колонтитула';
$_['entry_head_name_desc']    			= 'Заголовок верхнего колонтитула.';
$_['entry_display_title_module']       	= 'Показать заголовок модуля';
$_['entry_display_title_module_desc']   = 'Показать заголовок модуля.';
$_['entry_status']     					= 'Статус';
$_['entry_status_desc']     			= 'Опубликовать / Отменить публикацию модуля';

// Options
$_['entry_module']     		= 'Общие настройки';
$_['entry_source_option']   = 'Исходные настройки';
$_['entry_product_option']  = 'Настройки продуктов';
$_['entry_image_option']    = 'Настройки изображений';
$_['entry_advanced_option']     = 'Рекомендуемые настройки';

// General Options
$_['entry_class_suffix']  		= 'Модуль суффикс Class';
$_['entry_class_suffix_desc']	= 'Суффикс Class для модуля.';
$_['entry_open_link']  			= 'Открыть ссылку';
$_['entry_open_link_desc']  	= 'Тип показывается когда вы нажимаете на ссылку.';
$_['entry_limit_tags']  		= 'Ограничить вкладки';
$_['entry_limit_tags_desc']		= 'Количество отображаемых вкладок.';
$_['entry_min_font_size']  		= 'Минимальный размер шрифта';
$_['entry_min_font_size_desc']	= '9px минимум.';
$_['entry_max_font_size']  		= 'Максимальный размер шрифта';
$_['entry_max_font_size_desc']	= '25px максимум.';
$_['entry_font_weight']  		= 'Толщина шрифта';
$_['entry_font_weight_desc']	= 'Выбрать толщину шрифта.';

 //Tabs Advanced 
$_['entry_pre_text']                	= 'Пред-текст';
$_['entry_pre_text_desc']               = 'Верхний колонтитул модуля';
$_['entry_post_text']               	= 'Пост-текст';
$_['entry_post_text_desc']              = 'Нижний колонтитул модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля ';
$_['entry_cache_time']                  = 'Время кеширования';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед повторным кешированием модуля.';
// Help
$_['help_product']     					= '(Автозаполнение)';

// Error
$_['error_warning']          	= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			= 'Внимание: У вас нет разрешения на изменение встроенного модуля !';
$_['error_name']       			= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	= 'Имя заголовка модуля должно быть от 3 до 64 символов !';
$_['error_limit_tags'] 			= 'The number character format is required in this limit tags !';
$_['error_min_font_size'] 		= 'Числовые данные больше, чем 9 !';
$_['error_max_font_size'] 		= 'Числовые данные меньше, чем 25 !';
