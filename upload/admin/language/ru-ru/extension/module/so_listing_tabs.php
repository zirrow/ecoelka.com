<?php
// Heading
$_['heading_title']    = 'So Листинг вкладок';
$_['heading_title_so']    = 'So Листинг вкладок <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 2.2.0</b>';
$_['entry_button_clear_cache']  = 'Сбросить кеш';
// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Готово: Вы изменили so модуль Листинг вкладок!';
$_['text_success_remove']       = 'Готово: Кеш сброшен успешно!';
$_['text_edit']        = 'Редактировать So модуль Листинг вкладок';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактировать модуль ';
$_['text_show']        = 'Показать';
$_['text_hide']        = 'Спрятать';
$_['text_layout']      = 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и Редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и Создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';

//Default
$_['entry_include']     = 'Добавить';
$_['entry_exclude']     = 'Убрать';

// value
$_['value_blank']			= 'Новое окно';
$_['value_self']			= 'Такое же окно';
$_['value_loadmore']		= 'Загрузить больше';
$_['value_slider']			= 'Слайдер';
$_['value_name']       		= 'Название';
$_['value_model']       	= 'Модель';
$_['value_price']       	= 'Цена';
$_['value_quantity']       	= 'Количество';
$_['value_rating']       	= 'Рейтинг';
$_['value_sort_order']      = 'Порядок сортировки';
$_['value_date_added']      = 'Дата добавления';
$_['value_sell']       		= 'Хиты продаж';
$_['value_asc']       		= 'По возрастанию';
$_['value_desc']       		= 'По убыванию';
$_['value_cat_name']       	= 'Название';
$_['value_cat_lft']       	= 'Рандом';
$_['value_cat_random']      = 'Заказ';

// Name tab
$_['entry_module']            = 'Общие настройки';
$_['entry_source_option']     = 'Исходные настройки';
$_['entry_tabs_option']       = 'Настройки вкладок';
$_['entry_items_option']      = 'Настройки товаров';
$_['entry_image_option']      = 'Настройки изображений';
$_['entry_effect_option']     = 'Настройки эффектов';
$_['entry_advanced_option']     = 'Рекомендуемые настройки';
// Top config
$_['entry_name']              = 'Название модуля';
$_['entry_name_desc']         = 'Модуль должен иметь название';
$_['entry_head_name']         = 'Заголовок верхнего колонтитула';
$_['entry_head_name_desc']    = 'Озаглавить верхний колонтитул';
$_['entry_display_title_module']     = 'Показать заголовок модуля';
$_['entry_display_title_module_desc']= 'Показать заголовок модуля.';
$_['entry_status']     = 'Статус';
$_['entry_status_desc']     = 'Опубликовать / Отменить публикацию модуля';

//Tabs General
$_['entry_class_suffix']  			= 'Модуль суфикс Class';
$_['entry_class_suffix_desc']  		= 'Суфикс Class для модуля.';
$_['entry_open_link']  				= 'Открыть ссылку';
$_['entry_open_link_desc']  		= 'Тип отображается, когда вы нажимаете на ссылку';
$_['entry_column']     				= '# Колонки';
$_['entry_nb_column0_desc'] = 'Для устройств с шириной экрана >= 1200px и больше.';
$_['entry_nb_column1_desc'] = 'Для устройств с шириной экрана от >= 992px до < 1200px.';
$_['entry_nb_column2_desc'] = 'Для устройств с шириной экрана от >= 768px до < 992px.';
$_['entry_nb_column3_desc'] = 'Для устройств с шириной экрана от >= 480px до < 768px ';
$_['entry_nb_column4_desc'] = 'Для устройств с шириной экрана от < 480px ';
$_['entry_nb_row'] 			= '# Ряды';
$_['entry_nb_row_desc'] 	= 'Количество рядов ';
$_['entry_type_show'] 		= 'Тип показа';
$_['entry_type_show_desc'] 	= 'Выбрать тип.';


//Tabs Source options
$_['entry_type_source']           		= 'Тип источника';
$_['entry_type_source_desc']       		= 'Тип источника';
$_['entry_field_product']         		= 'Область продукта' ;
$_['entry_category']              		= 'Категория';
$_['entry_category_desc']   	  		= 'Выбрать категорию';
$_['entry_child_category']        		= 'Категория продуктов для детей';
$_['entry_child_category_desc']  		= 'Добавить или убрать продукты из категории товаров для детей.';
$_['entry_category_depth']        		= 'Глубина категории';
$_['entry_category_depth_desc']  		= 'Количество уровней категории для детей для возврата.';
$_['entry_product_order']         		= 'Сортировать товары по';
$_['entry_product_order_desc']			= 'Сортировка товаров';
$_['entry_ordering']              		= 'Направление заказа';
$_['entry_ordering_desc']   			= 'Выберите направление, по которому вы хотите сортировать товары.';
$_['entry_source_limit']          		= 'Подсчет';
$_['entry_source_limit_desc'] 			= 'Количество отображаемых продуктов. Значение по умолчанию 0 отображает все продукты.';
$_['entry_field_product_tab']     		= 'Область статьи для показа вкладок';
$_['entry_field_product_tab_desc']  	= 'Область статьи для показа вкладок';
$_['entry_field_preload']         		= "Предзагрузка области";
$_['entry_field_preload_desc']      	= "Предзагрузка области";
$_['entry_category_preload']      		= 'Предварительная загрузка категории';
$_['entry_catid_preload_desc']      	= 'Выбрать категорию';
$_['entry_for_preload']					= 'Для предварительной загрузки';
$_['entry_all_product']					= 'Все';

//Tabs Tabs options
$_['entry_tab_all_display']       		= 'Показать все вкладки'; 
$_['entry_tab_all_display_desc']       	= 'Показать все вкладки';
$_['entry_tab_max_characters']   		= 'Максимальная длина заголовка категории';
$_['entry_tab_max_characters_desc']   	= 'Максимальная длина заголовка по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_tab_icon_display']      		= 'Отобразить иконки';
$_['entry_tab_icon_display_desc']      	= 'Разрешить отображение / скрытие иконок';
$_['entry_cat_order_by']          		= 'Сортировать категории по';
$_['entry_cat_order_by_desc']          	= 'Выберите по какому полю вы хотите отсортировать Статьи. Рекомендуемые заказы должны быть использованы только когда Опция сортировки для Рекомендуемых Статей переключена на "Единственный".';
$_['entry_imgcfgcat_from_params'] 		= 'Параметры изображений';
$_['entry_imgcfgcat_from_params_desc'] 	= 'Параметры изображений';
$_['entry_imgcfgcat_from_description'] 	= 'Описание изображений';
$_['entry_imgcfgcat_width'] 			= 'Ширина изображения';
$_['entry_imgcfgcat_width_desc'] 		= 'Ширина изображения';
$_['entry_imgcfgcat_height'] 			= 'Высота изображения';
$_['entry_imgcfgcat_height_desc'] 		= 'Высота изображения';


//Tabs Items options
$_['entry_display_title']         		= 'Отобразить заголовок';
$_['entry_display_title_desc']   		= 'Отобразить заголовок товара';
$_['entry_title_maxlength']       		= 'Максимальная длина заголовка';
$_['entry_title_maxlength_desc']    	= 'Максимальная длина заголовка по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_display_description']   		= 'Отобразить описание';
$_['entry_display_description_desc']    = 'Отобразить описание товара';
$_['entry_description_maxlength'] 		= 'Максимальная длина описания';
$_['entry_description_maxlength_desc']  = 'Максимальная длина описания по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_display_price']     			= 'Отобразить цену';
$_['entry_display_price_desc']     		= 'Разрешить да / нет цены на продукт.';
$_['entry_display_add_to_cart']     	= 'Отобразить Добавить в корзину';
$_['entry_display_add_to_cart_desc']    = 'Отобразить кнопку Добавить в корзину.';
$_['entry_display_wishlist']     		= 'Отобразить Список желаемого';
$_['entry_display_wishlist_desc']     	= 'Отобразить кнопку списка желаемого.';
$_['entry_display_compare']     		= 'Отобразить Сравнение';
$_['entry_display_compare_desc']     	= 'Отобразить кнопку сравнения.';
$_['entry_display_rating']     			= 'Отобразить Рейтинг';
$_['entry_display_rating_desc']     	= 'Разрешить да / нет рейтинг на продукт.';
$_['entry_display_sale']     	    	= 'Отобразить распродажа';
$_['entry_display_sale_desc']        	= 'Отобразить распродажа';
$_['entry_display_new']     	    	= 'Отобразить новое';
$_['entry_display_new_desc']        	= 'Отобразить новое';
$_['entry_date_day']     	    		= 'Дата нового';
$_['entry_date_day_desc']        		= 'Дата дня для нового товара';
$_['entry_product_image_num']         	= 'Номер изобраения продукта';
$_['entry_product_image_num_desc']     	= 'Показать номер изображения для продукта'; 

//Tabs Images options	
$_['entry_product_image']     			= 'Изображение продукта';
$_['entry_product_image_desc']     		= 'Полное описание продукта';
$_['entry_product_get_image_data']     	= 'Получить изображение из данных вкладки';
$_['entry_product_get_image_data_desc'] = 'Получить изображение продукта из изображений данных вкладки.';
$_['entry_product_get_image_image']    	= 'Получить изображение из вкладки Изображение';
$_['entry_product_get_image_image_desc'] = 'Получить изображение продукта из вкладки Изображение.';
$_['entry_width'] 						= 'Ширина';
$_['entry_width_desc'] 					= 'Ширина изображения.';
$_['entry_height'] 						= 'Высота'; 
$_['entry_height_desc'] 				= 'Высота изображения.';
$_['entry_product_placeholder_path'] 	= 'Путь к изображению (или Url)'; 
$_['entry_product_placeholder_path_desc']      	= 'Путь к изображению по умолчанию (или  Url). Например: catalog/logo.png';
$_['entry_display_banner_image'] 		= 'Отбразить Изображение баннера'; 
$_['entry_display_banner_image_desc']	= 'Отобразить Изображение баннера.'; 
$_['entry_banner_image'] 				= 'Изображение баннера'; 
$_['entry_banner_image_desc']			= 'Изображение баннера.'; 
$_['entry_banner_image_url'] 			= 'Ссылка на изображение баннера'; 
$_['entry_banner_image_url_desc']		= 'Ссылка на изображение баннера.';
$_['entry_banner_width'] 				= 'Ширина изображения баннера';
$_['entry_banner_width_desc'] 			= 'Ширина изображения баннера.';
$_['entry_banner_height'] 				= 'Высота изображения баннера'; 
$_['entry_banner_height_desc'] 			= 'Высота изображения баннера.';

//Tabs Effect options
$_['effect_none'] 						= 'Нет';
$_['effect_fadeIn'] 					= 'Ослабить';
$_['effect_zoomIn'] 					= 'Приблизить';
$_['effect_zoomOut'] 					= 'Отдалить';
$_['effect_slideLeft'] 					= 'Влево';
$_['effect_slideRight'] 				= 'Вправо';
$_['effect_slideTop'] 					= 'Вверх';
$_['effect_slideBottom'] 				= 'Вниз';
$_['effect_flip'] 						= 'Повернуть';
$_['effect_flipInX'] 					= 'Повернуть по горизонтали';
$_['effect_flipInY'] 					= 'Повернуть по вертикали';
$_['effect_starwars'] 					= 'Star war';
$_['effect_bounceIn'] 					= 'Перетащить';
$_['effect_bounceInUp'] 				= 'Перетащить вверх';
$_['effect_bounceInDown'] 				= 'Перетащить вниз';
$_['effect_pageTop'] 					= 'Вверх страницы';
$_['effect_pageBottom'] 				= 'Вниз страницы';
$_['entry_effect']                		= 'Эффект';
$_['entry_effect_desc']           		= 'Выберите эффект для модуля здесь.';
$_['entry_duration']              		= 'Продолжительность';
$_['entry_duration_desc']             	= 'Определение продолжительности анимации. Больше = медленнее.';
$_['entry_delay']                 		= 'Задержка';
$_['entry_delay_desc']                 	= 'Устанавливить таймер для задержки выполнения следующего элемента в очереди. Больше = медленнее.';
$_['entry_autoplay']					= 'Автовоспроизведение';
$_['entry_autoplay_desc']				= 'Автовоспроизведение.';
$_['entry_display_nav']					= 'Отобразить навигацию';
$_['entry_display_nav_desc'] 			= 'Разрешить отображение / скрытие навигации для слайдера';
$_['entry_display_loop'] 				= 'Отобрзить петлю';
$_['entry_display_loop_desc'] 			='Бесконечная петля. Дублируйте последние и первые элементы, чтобы получить иллюзию петли.';

$_['entry_touchdrag'] 					= 'Touch перетаскивание';
$_['entry_touchdrag_desc'] 				= 'Touch перетаскивание включено';
$_['entry_mousedrag'] 					= 'Перетаскивание мышью';
$_['entry_mousedrag_desc'] 				= 'Перетаскивание мышью включено';
$_['entry_pausehover'] 					= 'Пауза при наведении';
$_['entry_pausehover_desc'] 			= 'Пауза при наведении мыши';
$_['entry_autoplayTimeout'] 			= 'Автоматический интервал ожидания';
$_['entry_autoplayTimeout_desc'] 		= 'Тайм-аут интервала автовоспроизведения для слайдера.';
$_['entry_autoplaySpeed'] 				= 'Скорость автовоспроизведения';
$_['entry_autoplaySpeed_desc'] 			= 'Скорость автовоспроизведения.';
$_['entry_for_type_show_load_more']		= 'Для типа показать Загрузить больше';
$_['entry_for_type_show_slider']		= 'Для типа показать Слайдер';

//Tabs Advanced 
$_['entry_pre_text']                	= 'Пред-текст';
$_['entry_pre_text_desc']               = 'Верхний колонтитул модуля';
$_['entry_post_text']               	= 'Пост-текст';
$_['entry_post_text_desc']              = 'Нижний колонтитул модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля ';
$_['entry_cache_time']                  = 'Время кеширования';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед повторным кешированием модуля.';

// Help
$_['help_product']     = '(Автозаполнение)';

$_['type_show_loadmore'] = 'Загрузить больше';
$_['type_show_slider'] = 'Слайдер';
// Error
$_['error_warning']          	= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			= 'Внимание: У вас нет разрешения на изменение встроенного модуля!';
$_['error_name']       			= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	= 'Имя заголовка модуля должно быть от 3 до 64 символов!';
$_['error_category']   			= 'Это поле не должно быть пустым!';
$_['error_category_depth']  	= 'The number character format is required in this category depth!';
$_['error_source_limit'] 		= 'The number character format is required in this limitation!';
$_['error_tab_max_characters'] 	= 'The number character format is required in this title maxlength!';
$_['error_description_maxlength'] = 'The number character format is required in this description maxlength!';
$_['error_title_maxlength'] 	= 'The number character format is required in this title maxlength!';
$_['error_field_product_tab']   = 'Это поле не должно быть пустым!';
$_['error_banner_image']   = 'Это поле не должно быть пустым!';
$_['error_imgcfgcat_width']     = 'The number character format is required in this width!';
$_['error_imgcfgcat_height']    = 'The number character format is required in this height!';
$_['error_width']     			= 'The number character format is required in this width!';
$_['error_height']     			= 'The number character format is required in this height!';
$_['error_duration']     		= 'The number character format is required in this duration!';
$_['error_delay']     			= 'The number character format is required in this delay!';
$_['error_nb_row']     			= 'The number character format is required in this row!';
$_['error_autoplayTimeout']     = 'The number character format is required in this autoplay Timeout!';
$_['error_autoplaySpeed']     	= 'The number character format is required in this autoplay Speed!';
$_['error_product_placeholder_path']    = 'Путь к изображению должен войти в изображение, например: nophoto.png';
$_['error_date_day']  			= 'The number character format is required in this date new!';

// value
$_['value_default']   		= 'По умолчанию';
$_['value_default2']   		= 'По умолчанию2';
$_['value_default3']   		= 'По умолчанию3';
$_['value_category']   		= 'Категория Избранные';
$_['entry_store_layout']                = 'Макет магазина';
$_['entry_store_layout_desc']           = 'Выберите Показать расположение магазина';