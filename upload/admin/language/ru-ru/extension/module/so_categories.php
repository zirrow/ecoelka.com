﻿<?php
// Heading
$_['heading_title']                 = 'So Категории';
$_['heading_title_so']              = 'So Категории <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 2.2.0</b>';
$_['entry_button_clear_cache']      = 'Сбросить кеш';
// Text
$_['text_module']      			    = 'Модули';
$_['text_success']     			    = 'Модуль обновлен!';
$_['text_success_remove']           = 'Кешь сброшен!';
$_['text_edit']        			    = 'Редактировать модуль So Категории';
$_['text_create_new_module']	    = 'Создать новый модуль';
$_['text_edit_module'] 			    = 'Редактировать модуль ';
$_['text_show']        			    = 'Показать';
$_['text_hide']        			    = 'Скрыть';
$_['text_layout']                   = 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и Редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить этот';

// value
$_['value_blank']			        = 'Новое окно';
$_['value_self']			        = 'Это же окно';
$_['value_theme1']			        = 'Тема 1';
$_['value_theme2']			        = 'Тема 2';
$_['value_theme3']			        = 'Тема 3';
$_['value_theme4']			        = 'Тема 4';
$_['value_click']			        = 'Клик';
$_['value_mouseenter']		        = 'Наведение';
$_['type_click']		            = 'Клик';
$_['type_hover']		            = 'Наведение';
//Default
$_['entry_include']                 = 'Вставить';
$_['entry_exclude']                 = 'Убрать';

// Entry
$_['entry_name']                    = 'Название модуля';
$_['entry_name_desc']               = 'Модуль должен иметь название';
$_['entry_head_name']               = 'Заголовок';
$_['entry_head_name_desc']          = 'Название заголовка';
$_['entry_display_title_module']    = 'Показать заголовок';
$_['entry_display_title_module_desc']   = 'Показывать заголовок модуля.';
$_['entry_status']     				= 'Статус';
$_['entry_status_desc']     		= 'Опубликовать / Отменить публикацию модуля';

$_['entry_module']            	    = 'Общие настройки';
$_['entry_source_option']     	    = 'Настрйоки поиска';
$_['entry_tabs_option']       	    = 'Параметры вкладки';
$_['entry_category_option']   	    = 'Опции категории';
$_['entry_product_option']  	    = 'Опции продукта';
$_['entry_image_option']      	    = 'Опции картинки';
$_['entry_effect_option']     	    = 'Настройка эффекта';
$_['entry_advanced_option']         = 'Дополнительный опции';

//Tabs General
$_['entry_open_link']  				= 'Открыть ссылку';
$_['entry_open_link_desc']  		= 'Тип отображается, когда вы нажимаете на ссылку';
$_['entry_deviceclass_sfx']     	= 'Название уникального CSS класса';
$_['entry_deviceclass_sfx_desc']    = 'Суффикс класса устройства.';
$_['entry_column']     				= 'Количество столбцов';
$_['entry_nb_column0_desc']         = 'Для устройств с шириной экрана от >= 1200px до максимума.';
$_['entry_nb_column1_desc']         = 'Для устройств с шириной экрана от >= 992px до < 1200px.';
$_['entry_nb_column2_desc']         = 'Для устройств с шириной экрана от >= 768px до < 992px.';
$_['entry_nb_column3_desc']         = 'Для устройств с шириной экрана от >= 480px до < 768px ';
$_['entry_nb_column4_desc']         = 'Для устройств с шириной экрана от < 480px ';
$_['entry_theme']     				= 'Тема';
$_['entry_theme_desc']     			= 'Выбор темы';
$_['entry_accmouseenter']     		= 'События вкладки';
$_['entry_accmouseenter_desc']     	= 'Выберите события';

//Tabs Source options
$_['entry_category']              	= 'Категория';
$_['entry_category_desc']         	= 'Выберите категорию';
$_['entry_child_category']        	= 'Дочерняя категория';
$_['entry_child_category_desc']  	= 'Добавить или убрать из категории.';
$_['entry_category_depth']        	= 'Глубина категории';
$_['entry_category_depth_desc']  	= 'Количество возвращаемых подкатегорий.';
$_['entry_source_limit']          	= 'Количество';
$_['entry_source_limit_desc'] 		= 'Количество отображаемых подкатегорий. Значение по умолчанию 0 отображает все подкатегории.';

$_['entry_category_options'] 		        = 'Опции категории:';
$_['entry_sub_category_options'] 	        = 'Опции под категории:';
//Tabs Items options
$_['entry_category_option__'] 			    = 'Опции категории:';
$_['entry_sub_category_option__'] 		    = 'Опции опд категории:';
$_['entry_cat_title_display']         		= 'Показать название категории';
$_['entry_cat_title_display_desc']      	= 'Показать название категории';
$_['entry_cat_title_maxcharacs']       		= 'Количество символов названия категории';
$_['entry_cat_title_maxcharacs_desc']      	= 'Максимальное количество символов названия категорий. 0 для отключения ограничения длины!';
$_['entry_cat_sub_title_display'] 			= 'Отображеть заголовк подкатегории';
$_['entry_cat_sub_title_display_desc'] 		= 'Показать заголовок подкатегории';
$_['entry_cat_sub_title_maxcharacs'] 		= 'Количетво символов названия подкатегории';
$_['entry_cat_sub_title_maxcharacs_desc'] 	= 'Максимальное количество символов названия подкатегорий. 0 для отключения ограничения длины!';
$_['entry_cat_all_product'] 				= 'Показать количество продуктов';
$_['entry_cat_all_product_desc'] 			= 'Показать количество продуктов';
$_['entry_display_rating']     				= 'Отображать рейтинг';
$_['entry_display_rating_desc']     		= 'Разрешить да/нет отображение оценки продукта.';
$_['entry_display_price']     				= 'Отображать цену';
$_['entry_display_price_desc']     			= 'Разрешить да/нет отображение цены продукта.';
$_['entry_display_add_to_cart']     		= 'Отображать "добавить в корзину"';
$_['entry_display_add_to_cart_desc']    	= 'Отображать кнопку "добавить в корзину"';
$_['entry_display_wishlist']     	    	= 'Отображать "список желаемых"';
$_['entry_display_wishlist_desc']       	= 'Отображать кнопку "список желаемых"';
$_['entry_display_compare']     	    	= 'Отображать "ставнение"';
$_['entry_display_compare_desc']        	= 'Отображать кнопку "Сравнение товаров"';
$_['entry_display_sale']     	    		= 'Отображать "распродажа"';
$_['entry_display_sale_desc']        		= 'Отображать "распродажа"';
$_['entry_display_new']     	    		= 'Отображать "новинка"';
$_['entry_display_new_desc']        		= 'Отображать "новинка"';
$_['entry_date_day']     	    			= 'Время "новинки"';
$_['entry_date_day_desc']        			= 'Количество дней которые прдукт будет счиаться "новинкой"';
//Tabs Images options
$_['entry_product_image']         		= 'Изображение категории';
$_['entry_product_image_desc']     		= 'Изображение категории';
$_['entry_width']                 		= 'Ширина';
$_['entry_width_desc'] 					= 'Ширина картинки';
$_['entry_height']                		= 'Высота';
$_['entry_height_desc'] 				= 'Высота картинки';
$_['entry_placeholder_path']      		= 'Путь (или Url)';
$_['entry_placeholder_path_desc']      	= 'Путь (или URL) К картинке по умолчанию. Например: catalog/logo.png';
//Tabs Advanced 
$_['entry_pre_text']                	= 'Пред. текст';
$_['entry_pre_text_desc']               = 'Вступительный текст для модуля';
$_['entry_post_text']               	= 'Пост. текст';
$_['entry_post_text_desc']              = 'Футер в модуле';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля ';
$_['entry_cache_time']                  = 'Время кеширвоания';
$_['entry_cache_time_desc']             = 'Время (в секундах) через сколько модуль перекешируется.';
// Help
$_['help_product']                      = '(Автодополнение)';
$_['entry_status']                      = 'Статус';
// Error
$_['error_warning']          	        = 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission']                  = 'Внимание: У вас нет разрешения на изменение модуля категорий so!';
$_['error_name']                        = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	        = 'Имя заголовка модуля должно быть от 3 до 64 символов!';
$_['error_category']   			        = 'Это поле не должно быть пустым!';
$_['error_category_depth']  	        = 'В этой категории требуется формат номера символов.!';
$_['error_source_limit'] 		        = 'В этом ограничении требуется формат номера символа!';
$_['error_cat_title_maxcharacs'] 	    = 'Формат символа номера требуется в этом заголовке max chars!';
$_['error_cat_sub_title_maxcharacs']    = 'Формат символа номера требуется в этом заголовке max chars!';
$_['error_width']      			        = 'The number character format is required in this width!';
$_['error_height']     			        = 'The number character format is required in this height!';
$_['error_placeholder_path']            = 'Строка выбора пути должна входить в изображение, например: nophoto.png';
$_['error_date_day']  			        = 'The number character format is required in this date new!';