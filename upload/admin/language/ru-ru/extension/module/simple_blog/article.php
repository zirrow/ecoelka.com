<?php
// Heading
$_['heading_title']      	    = 'Статья';

// Text
$_['text_success']			    = 'Успех: вы изменили статьи!';
$_['text_browse']			    = 'Просмотреть';
$_['text_clear']			    = 'Очистить';
$_['text_default']			    = 'По умолчанию';
$_['text_image_manager']	    = 'Менеджер картинок';
$_['text_add']	                = 'Добавить статью';
$_['text_edit']	                = 'Редактировать статью';
$_['text_keyword']	            = 'Не испольщуйте пробелы, вместо этого замените их тире "-", и убедитесь что SEO URL уникален.';

// Buttons
$_['button_add_description']    = 'Добавить описание';
$_['button_add_articles']	    = 'Добавить статью';

// Columns
$_['column_article_title']	    = 'Title';
$_['column_author_name']	    = 'Имя автора';
$_['column_sort_order']		    = 'Порядок сортировки';
$_['column_status']			    = 'Статус';
$_['column_date_added']		    = 'Дота добавления';
$_['column_action']			    = 'Действие';

// Tab
$_['tab_data']				    = 'Контент';
$_['tab_related']			    = 'Связанные';
$_['tab_design']			    = 'Дизайн';

// Help
$_['help_title']                = 'Title статьи должен быть уникальным.';
$_['help_author_name']          = 'Имя автора должно быть выбрано из автокомплита.';
$_['help_image']                = 'Это изображение главной статьи.';
$_['help_featured_image']       = 'Это будет отображаться как главная картинка статьи на странице статей и в списке статей.';
$_['help_main_image']           = 'Это будет отображаться в последних статьях.';

$_['help_productwise']              = '(Автодополнение)';
$_['help_article_related_method']   = 'Эта статья привязанна к продукту, списку продуктов или производителю. Выберите в зависимости от требования.';
$_['help_related_article_name']     = 'Название статьи должно быть выбрано из автокомплита.';


// Entry
$_['entry_title']       	    = 'Название статьи:';
$_['entry_description']    	    = 'Описание:';
$_['entry_author_name']    	    = 'Имя автора:';
$_['entry_meta_description']    = 'Meta Description:';
$_['entry_meta_keyword']	    = 'Meta keyword:';
$_['entry_allow_comment']	    = 'Разрешить комментирование?:';
$_['entry_keyword']     	    = 'Ключевики';
$_['entry_image']     		    = 'картинка:';
$_['entry_featured_image']      = 'Сопутствующие картинки:';
$_['entry_main_image']          = 'Главное изображение:';

$_['entry_sort_order']          = 'Порядок сортировки:';
$_['entry_status']      	    = 'Статус:';
$_['entry_category']      	    = 'Категории:';
$_['entry_manufacturer']        = 'Производитель:';
$_['entry_product']    		    = 'Продукт:';
$_['entry_productwise']   	    = 'Название продукта:';
$_['entry_store']      		    = 'Магазин';
$_['entry_layout']      	    = 'Переопределение макета:';
$_['entry_additional_description'] = 'Дополнительное описание';
$_['entry_article_related_method'] = 'Привязка:';
$_['entry_blog_related_articles']  = 'Связанные статьи';
$_['entry_category_wise']       = 'К категории';
$_['entry_manufacturer_wise']   = 'К производителю';
$_['entry_product_wise']   	    = 'К продукту';
$_['entry_related_article_name'] = 'Название статьи:';

// Error
$_['error_warning']			= 'Warning: Please check the form carefully for errors!';
$_['error_permission']  	= 'Warning: You do not have permission to modify article!';
$_['error_title']       	= 'Article Title must be greater than 3 and less than 100 characters!';
$_['error_title_found']     = 'Article Title already exists, article title must be unique!';
$_['error_description'] 	= 'Description must be greater than 3 characters!';
$_['error_author_name'] 	= 'Author name can\'t be blank!';
$_['error_author_not_found_list'] = 'Author not found in our list!';
$_['error_author_not_found']= 'Sorry Author not found in our list, please insert valid author name!';
$_['error_seo_not_found']   = 'Seo Keyword must be between 3 to 64 characters!';
$_['error_article_related']	= 'Warning: You can\'t delete article because currently it relate %s articles!';
$_['error_keyword']         = 'SEO URL already in use!';
$_['error_unique']          = 'SEO URL must be unique!';

$_['button_insert']			= 'Вставить';
