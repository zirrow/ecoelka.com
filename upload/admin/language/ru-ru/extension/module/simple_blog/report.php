<?php
// Heading
$_['heading_title']  		= 'Отчет по просмотрам';

// Text
$_['text_success']   		= 'Success: You have modified the blog viewed report!';

// Column
$_['column_article_name']	= 'Название статьи';
$_['column_author_name']	= 'Имя автора';
$_['column_viewed']  		= 'Просмотры';
$_['column_percent'] 		= 'Процент';

// Entry
$_['entry_date_start']  	= 'Дата начала:';
$_['entry_date_end']    	= 'Дата конца:';
$_['button_insert']			= 'Встравить';

