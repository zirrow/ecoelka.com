<?php
// Heading
$_['heading_title']      	= 'Авторы';

// Columns
$_['column_author_name']	= 'Имя автора';
$_['column_status']			= 'Статус';
$_['column_date_added']		= 'Дата добавления';
$_['column_action']			= 'Действие';

// Text
$_['text_success']       	= 'Успех: вы изменили авторов блога!';
$_['text_default']       	= 'По умолчанию';
$_['text_image_manager'] 	= 'Менеджер картинок';
$_['text_browse']        	= 'Просмотреть';
$_['text_clear']         	= 'Очистить';
$_['text_add']	            = 'Добавить автора';
$_['text_edit']	            = 'Редактировать автора';
$_['text_keyword']	        = 'Не используйте пробелы, вместо этого замените пробелы на - и убедитесь, что SEO URL уникален.';

// Help
$_['help_name']             = 'Имя автора должно быть уникальным.';
$_['help_keyword']          = 'Не используйте пробелы, вместо этого замените пробелы на - и убедитесь, что keyword уникален.';
$_['help_adminid']          = 'имя пользователя учетной записи администратора - см. документацию';

// Entry
$_['entry_name']         	= 'Имя автора:';
$_['entry_keyword']      	= 'Keyword';
$_['entry_image']        	= 'Аватар:';
$_['entry_type']         	= 'Тип:';
$_['entry_adminid']      	= 'Id Администратора:';
$_['entry_description']  	= 'Описание:';
$_['entry_ctitle']       	= 'Пользовательское название страницы:';
$_['entry_meta_description']= 'Meta Tag Description:';
$_['entry_meta_keyword'] 	= 'Meta Tag Keywords:';
$_['entry_status'] 			= 'Статус:';
$_['entry_store'] 			= 'Магазин';


// Errors
$_['error_warning']			= 'Warning: Please check form carefully for errors!';
$_['error_permission']   	= 'Warning: You do not have permission to modify the authors!';
$_['error_name']         	= 'Author Name must be between 3 and 255 characters!';
$_['error_author_found']    = 'Author Name already exists, author name must unique!';
$_['error_seo_not_found']   = 'Seo Keyword must be between 3 to 64 characters!';
$_['error_article']      	= 'Warning: This Author cannot be deleted as it is currently assigned to %s articles!';
$_['button_insert']			= 'Вставить';
$_['error_keyword']         = 'SEO URL уже используется!';
$_['error_unique']          = 'SEO URL должен быть уникальным!';

