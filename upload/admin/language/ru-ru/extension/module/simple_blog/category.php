<?php
// Heading
$_['heading_title']      	= 'Категории';

// Text
$_['text_success']			= 'Успех: вы изменили категории!';
$_['text_default']          = 'По умолчанию';
$_['text_image_manager']    = 'Менеджер картинок';
$_['text_browse']           = 'Выберать фаил';
$_['text_clear']            = 'Очистить картинку';
$_['text_add']	            = 'Добавить категорию';
$_['text_edit']	            = 'Редактировать категорию';
$_['text_keyword']	        = 'Не используйте пробелы, вместо этого замените пробелы - и убедитесь, что URL-адрес SEO уникален.';

// Column
$_['column_name']           = 'Название категории';
$_['column_sort_order']     = 'Порядок сортировки';
$_['column_status']      	= 'Статус';
$_['column_action']         = 'Действие';

// Help
$_['help_keyword']          = 'Ключ должен быть глобально уникален.';
$_['help_column']           = 'Количество столбцов для нижних 3 категорий. Работает только для основных родительских категорий.';
$_['help_article_limit']    = 'Количество статей, отображаемых на странице этой категории.';
$_['help_top']              = 'Отображение в верхней строке меню. Работает только для основных родительских категорий.';

// Entry
$_['entry_name']             = 'Название категории:';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'Название категории:';
$_['entry_parent']           = 'Название родительской категории:';
$_['entry_store']            = 'Магазин';
$_['entry_keyword']          = 'Keyword';
$_['entry_image']            = 'Картингка:';
$_['entry_column']   		 = 'Столбцы:';
$_['entry_article_limit']    = 'Количество статей на странице:';
$_['entry_sort_order']       = 'Порядок сортировки:';
$_['entry_status']           = 'Статус:';
$_['entry_top']				 = 'Top:';
$_['entry_layout']           = 'Переопределение макета:';

// Error 
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Category Name must be between 2 and 255 characters!';
$_['error_seo_not_found']    = 'Seo Keyword must be between 3 to 64 characters!';
$_['error_article']      	 = 'Warning: This Category cannot be deleted as it is currently assigned to %s articles!';
$_['error_keyword']          = 'SEO URL already in use!';
$_['error_unique']           = 'SEO URL must be unique!';

$_['button_insert']			 = 'Вставить';
