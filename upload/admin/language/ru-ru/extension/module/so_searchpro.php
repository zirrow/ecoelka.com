﻿<?php
// Heading
$_['heading_title']    		        = 'So Поиск Pro';
$_['heading_title_so']    	        = $_['heading_title'].' <p class="btn btn-info btn-xs">версия 3.0.0</p>';
$_['entry_button_clear_cache']      = 'Сбросить кеш';
// Text
$_['text_module']      		        = 'Модули';
$_['text_success']     		        = 'Готово: Вы изменили модуль So Search Pro!';
$_['text_success_remove']           = 'Готово: Кеш сброшен успешно!';
$_['text_edit']        		        = 'Редактирование модуля So Поиск Pro';
$_['text_layout']      		        = 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';
$_['text_str_keyword']     	        = 'Ключевое слово ';
// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и редактирвоать';
$_['entry_button_save_and_new']    	= 'Сохранить и создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';
$_['entry_show_keyword']            = 'Показать ключевое слово';
$_['entry_show_keyword_desc']       = 'Показать ключевое слово';
$_['entry_str_keyword']             = 'Текстовое ключевое слово';
$_['entry_str_keyword_desc']        = 'Текстовое ключевое слово';
$_['entry_limit_keyword']           = 'Limit Keyword';
$_['entry_limit_keyword_desc']      = 'Limit keyword';
$_['value_default']   		        = 'По умолчанию';
$_['value_default2']   		        = 'По умолчанию 2';
$_['value_default3']   		        = 'По умолчанию 3';
$_['value_default4']   		        = 'По умолчанию 4';
// Entry
$_['entry_status']     			    = 'Статус';
$_['entry_head_name']     		    = 'Название';
$_['entry_head_name_desc']     		= 'Модуль должен иметь название';
$_['entry_display_title_module']    = 'Показывать название';
$_['entry_showcategory']            = 'Show Form Category';
$_['entry_showimage']     	        = 'Показать картинку';
$_['entry_showprice']     	        = 'Показать цену';
$_['entry_showaddtowishlist']     	= 'Показать "добавить в список желаемых"';
$_['entry_showaddtocompare']     	= 'Показать "добавить в сравнения"';
$_['entry_name']     		        = 'Название модуля';
$_['entry_class_suffix']     		= 'Class суффикс';
$_['entry_class_suffix_desc']     	= 'Class суффикс';
$_['entry_width']     		        = 'Ширина картинки';
$_['entry_width_desc']     	        = 'Ширина картинки.';
$_['entry_height']     		        = 'Высота картинки';
$_['entry_height_desc']             = 'Высота картинки.';
$_['entry_limit']     		        = 'Лимит';
$_['entry_limit_desc']     		    = 'Лимит';
$_['entry_character']     	        = 'Символ';
$_['entry_character_desc']     	    = 'Символ';
$_['entry_name_desc']               = 'У модуля должно быть название';
$_['entry_button_add']     	        = 'Добавить модуль';
$_['entry_button_delete']     	    = 'Удалить этот модуль';
$_['entry_store_layout']            = 'Макет';
$_['entry_store_layout_desc']       = 'Выберите макет магазина';
$_['entry_use_cache']               = 'Кеширование';
$_['entry_use_cache_desc']          = 'Выберите, следует ли кэшировать содержимое этого модуля';
$_['entry_cache_time']              = 'Время кеширвоания';
$_['entry_cache_time_desc']         = 'Время (в секундах) перед повторным кешированием модуля.';

// Error
$_['error_permission']              = 'Внимание: у вас нет разрешения на изменение модуля So Search Pro!';
$_['error_width']                   = 'Требуется ширина!';
$_['error_height']                  = 'Требуется высота!';
$_['error_limit']                   = 'Требуется ограничение!';
$_['error_character']               = 'Требуется символ!';
$_['error_name']       			    = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       		= 'Заголовок модуля должен быть от 3 до 64 символов!';
$_['error_cache_time']              = 'Cache time required!';