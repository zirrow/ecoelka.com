<?php
$_['heading_title_normal']  = 'Мобильная панель управления ';

$_['heading_title'] 		= '<i class="fa fa-mobile" style="размер шрифта: 18px;поле: 0 5px;отступы: 5px;фон: #1e91cf;цвет: белый; граница радиуса: 3px; ширина:24px;выравнивание текста:по центру;"></i> Мобильная панель управления';

$_['text_module']     		= 'Модули';
$_['module_intro'] 			= 'Отредактируйте мобильные функции SO здесь';
$_['theme_version']         = 'Версия 1.0.0';


// Text
$_['text_success']          = 'Готово: Вы изменили модуль!';

// Error
$_['error_permission']      = 'Внимание: У вас нет разрешения на модификацию модуля!';
$_['error_nameColor']       = 'Это поле не должно быть пустым';
$_['error_warning']         = 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';

$_['text_content_top']      = 'Вверх';
$_['text_content_bottom']   = 'Вниз';
$_['text_column_left']      = 'Влево';
$_['text_column_right']     = 'Вправо';
$_['text_default']          = 'По умолчанию';
$_['text_image_manager']    = 'Менеджер изображений';
$_['text_browse']           = 'Просмотреть';
$_['text_clear']            = 'Очистить';

// Column
$_['column_slide_id']        = 'Slide ID';
$_['column_image_left']      = 'Изображение слева';
$_['column_image_right']     = 'Изображение справа';
$_['column_url_left']        = 'Ссылка слева';
$_['column_url_right']       = 'Ссылка справа';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'Действие';


// Entry
$_['entry_image_left']       = 'Изображение слева:';
$_['entry_image_right']      = 'Изображение справа:';
$_['entry_url_left']         = 'URL слева:';
$_['entry_url_right']        = 'URL справа:';
$_['entry_sort_order']       = 'Порядок сортировки:';
$_['entry_status']           = 'Статус:';
$_['entry_position']         = 'Размещение:';
$_['text_edit']           	 = 'Редактирование панели управления темами';

$_['text_yes']           				= 'Да';
$_['text_no']           				= 'Нет';
$_['entry_standard']           			= 'Стандарт';
$_['entry_google_font']           		= 'Шрифты Google';
$_['entry_custom_block']                = 'Пользовательский блок';

$_['entry_title_label']                 = 'Заголовок :'; 
$_['entry_custom_column']               = 'Содержание :';
$_['entry_my_account']                  = 'Ссылка на аккаунт :';
$_['entry_contact_us']                  = 'Ссылка на контакт :';
$_['entry_google_url']                  = 'Google URL :';
$_['entry_google_url_help']             = 'Пример: http://fonts.googleapis.com/css?family=Roboto:400,500,700 <a href="https://www.google.com/fonts">&#8658; Показать больше</a>';
$_['entry_google_family']               = 'Google Family :';
$_['entry_google_family_help']          = 'Пример: Roboto, sans-serif;';
$_['entry_catalog_column_help']         = '(установить количество столбцов для устройств)';


// Main_tabs Options
$_['maintabs_general']                 	= '<i class="fa fa-cog"></i> <span>Общий </span>';
$_['maintabs_products']                 = '<i class="fa fa-file-text"></i> Шрифты</span>';
$_['maintabs_advanced']                	= '<i class="fa fa-wrench"></i> <span>Рекомендуемый</span>';
$_['maintabs_social']                	= '<i class="fa fa-facebook"></i> <span>Социальные виджеты</span>';
$_['maintabs_sampledata']               = '<i class="fa fa-database"></i> <span>Пример данных</span>';
$_['maintabs_barbottom']                = '<i class="fa fa-list-ol" ></i><span>Нижняя панель</span>';
$_['maintabs_barleft']               	= '<i class="fa fa-align-left" ></i><span>Левая панель</span>';

// General Options
$_['general_tab_general']               = 'Общие';
$_['general_tab_header']                = 'Шапка';
$_['general_tab_footer']                = 'Футер';

$_['general_tab_mainmenu']              = 'Главное меню';
$_['general_tab_language']              = 'Смена языка';

// General Heading
$_['themecolor_heading']                = 'Цвет темы';
$_['platforms_heading']                 = 'Платформы';
$_['general_heading']                	= 'Общее';
$_['typeheader_heading']                = 'Тип заголовка';
$_['typeheader_dec']                	= 'Если вам нужен контент из другого заголовка, такой как номер телефона, Вам нужно установить его в пользовательский элемент управления темами ';
$_['typeheader_text']               	= 'Вы можете установить эти заголовки для любого скина';
$_['category_heading']                	= 'Страница категории';
$_['compile_heading']                	= 'Компиляция SCSS ';
$_['fonts_heading']                	    = 'Шрифты';
$_['barmore_heading']                	= 'Нижняя панель';
$_['barleft_heading']                	= 'Левая панель';

$_['general_createcolor']                = 'Создать цвет';
$_['general_stylecolor']                 = 'Стиль цвета';
$_['general_mobile_status']              = 'Мобильный статус';
$_['general_logo_mobile']                = 'Мобильный логотип';
$_['general_backtop_status']             = 'Вернуться к началу';
$_['general_topbar_status']              = 'Фиксированая верхняя панель ';
$_['general_copyright_text']             = 'Копирайт';
$_['barmore_status']             		 = 'Показать больше меню';
$_['bottombar_status']                	 = 'Показать нижнюю панель';

$_['barsearch_status']             		 = 'Показать поиск ';
$_['barcategory_status']             	 = 'Показать категорию ';
$_['barmega1_status']             	     = 'Показать главное меню ';
$_['barmega2_status']             	     = 'Показать все категории';
$_['barwistlist_status']             	 = 'Показать список пожеланий ';
$_['barcompare_status']             	 = 'Показать сравнение ';
$_['barcurenty_status']             	 = 'Показать валюту ';
$_['barlanguage_status']             	 = 'SПоказать язык ';

$_['general_morecategory_status']        = 'Больше кнопок категорий';
$_['general_compare_status']             = 'Сравнить кнопки';
$_['general_wishlist_status']            = 'Кнопка списка пожеланий';
$_['general_addcart_status']             = 'Кнопка Добавить в корзину';
$_['general_countdown_status']           = 'Показать обратный отсчет Специального предложения';

$_['compile_status']                    = 'Компиляция SCSS';
$_['cssformat_status']                	= 'CSS формат';
$_['muticolor_status']                	= 'User Developer Compile Muti Color ';

// Fonts Options
$_['fonts_body']                		= 'Настройка шрифта Body';
$_['fonts_menu']             			= 'Настройка шрифра меню';
$_['fonts_heading']                		= 'Настройка верхнего колонтитула';
$_['fonts_custome']                		= 'Настройка пользовательских шрифтов';

